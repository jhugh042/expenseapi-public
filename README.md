
## Building & Running

#### Requirements
* **Maven**
* **Java 8**
* **Docker**

1. Make sure you are in the directory of the project.
2. Create the jar file by running the command `mvn clean package`.
3. Start the docker containers with the command `docker-compose up` or `docker-compose up -d` for the detached version.
4. You can stop the Docker instances by running `docker-compose down` or `docker-compose stop`.

## Running from release

#### Requirements
* **Docker**

1. Download the project from the realases and unzip it.
2. Start the docker containers with the command `docker-compose up` or `docker-compose up -d` for the detached version.
3. You can stop the Docker instances by running `docker-compose down` or `docker-compose stop`.



# ExpensesAPI documentation version v1
http://localhost:8080/api/

---
## /api

### /api

* **get**:Returns an HTML page containing API documentation and endpoints.

## /api/masterExpense

### /api/masterExpense

* **get**: Get the sum of each expense category by month for the given year.
  - **parameters:**
    - *year: string*
    - *html?: boolean*

### /api/masterExpense/expenses

* **get**: Gets every expense currently listed in the database.
  - **parameters:**
    - *account?: string*
    - *category?: string*
    - *html?: boolean*
    - *gt?: integer*
    - *lt?: integer*
    - *year?: integer*
    - *month?: string*
    - *day?: integer*
* **post**: Creates a new expense.

### /api/masterExpense/expenses/{id}

* **get**: Get the expense with the given id.
  - **parameters:**
    - *html?: boolean*
* **patch**: Update the expense with the given id.
* **delete**: Remove the expense with the given id.

## /api/masterIncome

### /api/masterIncome

* **get**: Get the sum of each income account by month for the given year.
  - **parameters:**
    - *year: string*
    - *html?: boolean*

### /api/masterIncome/incomes

* **get**: Gets every income currently listed in the database.
  - **parameters:**
    - *account?: string*
    - *html?: boolean*
    - *gt?: integer*
    - *lt?: integer*
    - *year?: integer*
    - *month?: string*
    - *day?: integer*
* **post**: Create a new Income.

### /api/masterIncome/incomes/{id}

* **get**: Get the income with the given id.
  - **parameters:**
    - *html?: boolean*
* **patch**: Update the income with the given id.
* **delete**: Remove the income with the given id.

## /api/accounting

### /api/accounting

* **get**: Get accounting T book for given year and month.
  - **parameters:**
    - *account?: string*
    - *html?: boolean*
    - *year: integer*
    - *month: string*

### /api/accounting/paystubs

* **get**: Get all pay stub data.
  - **parameters:**
    - *html?: boolean*
    - *year: integer*
* **post**: Create a new paystub.
  - **parameters:**
    - *createIncome?: boolean*
    - *account?: string*

### /api/accounting/paystubs/{id}

* **get**: Get paystub with given ID.
  - **parameters:**
    - *html?: boolean*
* **patch**: Update paystub with given ID.
* **delete**: Remove paystub with given ID.

### /api/accounting/accounts

* **get**: Get a list of all accounts.
  - **parameters:**
    - *html?: boolean*
* **post**: Create a new account.

### /api/accounting/accounts/{id}

* **get**: Get a single account.
  - **parameters:**
    - *html?: boolean*
* **patch**: Update the account with given ID.
* **delete**: Remove the account with given ID.

### /api/accounting/loans

* **get**: Get a list of all loans.
  - **parameters:**
    - *html?: boolean*
* **post**: Create a new loan.

### /api/accounting/loans/{id}

* **get**: Get a single loan.
  - **parameters:**
    - *html?: boolean*
* **patch**: Update the loan with given ID.
* **delete**: Remove the loan with given ID.

### /api/accounting/loans/{id}/loanPayment

* **get**: Get a list of all loan payments.
  - **parameters:**
    - *html?: boolean*
* **post**: Create a new loan payment.

### /api/accounting/loans/{id}/loanPayment/{id}

* **get**: Get a single loan payment.
  - **parameters:**
    - *html?: boolean*
* **patch**: Update the loan payment with given ID.
* **delete**: Remove the loan payment with given ID.

### /api/accounting/loans/{id}/estimate
* **get**: Get a list of all loan payments.
  - **parameters:**
    - *html?: boolean*
    - *payment?: number*
    
    
