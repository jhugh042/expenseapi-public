INSERT INTO accounts(name, type)
VALUES ('Checking', 'Checking');

/*=============================================================================*/

INSERT INTO expenses(account, category, month, year, day, ammount)
VALUES (1,'Food','April',2019,14,178.98);

INSERT INTO expenses(account, category, month, year, day, ammount)
VALUES (1,'Other','April',2019,14,109.02);

INSERT INTO expenses(account, category, month, year, day, ammount)
VALUES (1,'Food','April',2019,15,5.85);


/*==========================Income==============================*/

INSERT INTO incomes(account, month, year, day, ammount)
VALUES (1,'May',2019,15,100.00);

INSERT INTO incomes(account, month, year, day, ammount)
VALUES (1,'May',2019,15,100.00);

INSERT INTO incomes(account, month, year, day, ammount)
VALUES (1,'May',2019,15,100.00);

INSERT INTO incomes(account, month, year, day, ammount)
VALUES (1,'May',2019,15,100.00);


/*=================================Paystub=======================*/

INSERT INTO paystub(month, year, day, gross_pay,fed_tax,state_tax,ssn_tax,medicare,retirement,net_pay)
VALUES ('April',2019,30,100.00,100.00,100.00,100.00,100.00,100.00,100.00);

INSERT INTO paystub(month, year, day, gross_pay,fed_tax,state_tax,ssn_tax,medicare,retirement,net_pay)
VALUES ('May',2019,30,100.00,100.00,100.00,100.00,100.00,100.00,100.00);

/*==================================Loan====================================*/

INSERT INTO loans(name,ammount,interest,loan_start_month,loan_start_year,loan_end_month,loan_end_year)
VALUES ('Test',10000.00,3.20,'June',2019,'June',2029);






















