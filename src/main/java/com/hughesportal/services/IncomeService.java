package com.hughesportal.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.hughesportal.entity.Incomes;
import com.hughesportal.objects.MasterIncome;
import com.hughesportal.objects.Months;
import com.hughesportal.objects.SingleMasterIncome;
import com.hughesportal.repositories.IncomeRepository;

/** 
 * @author Jacob Hughes
 *
 * Service for the IncomeController, handles
 * all of the business logic
 */
@Service
public class IncomeService {

	@SuppressWarnings("unused")
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	IncomeRepository incRepo;
	
	/**
	 * Get Master Income data
	 * @param year
	 * @param html
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> getMasterIncome(
									 Integer year,
									 Boolean html){
		if(year != null) { //year cannot be null
			
			List<SingleMasterIncome> incomes = incRepo.getSingleIncomeForMasterIncome(year);
			List<MasterIncome> master = new ArrayList<>();
			List<Integer> accounts = new ArrayList<>();
			
			//loop through all incomes
			for(SingleMasterIncome inc : incomes) {
				
				//category already exists
				if(accounts.contains(inc.getAccount())) {
					
					for(int i =0; i<master.size();i++) {
						
						if(master.get(i).getAccount().equals(inc.getAccount())) {
							logger.info("Adding ("+inc.getMonth()+", "+inc.getAmmount()+") to master");
							master.get(i).getMonths().setData(inc.getMonth(),inc.getAmmount());
						}
					}
				}
				//category does not exist yet
				else {
					//add the category
					accounts.add(inc.getAccount());
					master.add(
						new MasterIncome(
							inc.getAccount(), 
							new Months(inc.getMonth(), inc.getAmmount())));
				}
			}
			
			//return data as html table
			if(html != null && html == true) {
				
				//convert data to string
				String content = "";
				for(MasterIncome masterInc : master) {
					content += masterInc.toString();
				}
				
				return ResponseEntity.status(HttpStatus.OK)
						.body(BaseService.appendBody(MasterIncome.getTitle(),content));
			}
			//return json data
			else {
				return ResponseEntity.status(HttpStatus.OK)
						.body(master);
			}
		}
		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
				.header("Content-Type", "application/json")
				.body(BaseService.BAD_REQUEST("Missing year query parameter."));
	}
	
	/**
	 * Get all income data
	 * @param day
	 * @param gt
	 * @param lt
	 * @param month
	 * @param year
	 * @param account
	 * @param html
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> getAllIncomes(
								Integer day,
								Integer gt,
								Integer lt,
								String month,
								Integer year,
								Integer account,
								Boolean html){
		List<Incomes> incomes = incRepo.findByDayAndMonthAndYearAndAccount(day, month, year, account);
		
		if(lt != null) {
			incomes.removeIf(i -> i.getAmmount() > lt);
		}
		
		if(gt != null) {
			incomes.removeIf(i -> i.getAmmount() < gt);
		}
		
		if( html != null && html == true) {
			String content = "";
			for( Incomes inc : incomes) {
				content += inc.toString();
			}
			return ResponseEntity.status(HttpStatus.OK)
					.body(BaseService.appendBody(Incomes.getTitle(),content));
		}
		return ResponseEntity.status(HttpStatus.OK)
				.body(incomes);
	}
	
	/**
	 * Create a new income
	 * @param income
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> createIncome(Incomes income){
		
		Long id = null;
		boolean badData = true;
		if(income != null) {
			if(income.getAccount() != null &&
				income.getAmmount() != null &&
				income.getDay() != null &&
				income.getMonth() != null &&
				income.getYear() != null) {
				
				badData = false;
				
				id = incRepo.findById(
						incRepo.save(
							new Incomes(
									income.getMonth(),
									income.getYear(),
									income.getDay(),
									income.getAmmount(),
									income.getAccount())
							).getId()).get().getId();
			}
		}
		if(income == null || badData) {// post was null or bad data
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.header("Content-Type", "application/json")
					.body(BaseService.BAD_REQUEST("Error in your post body."));
		}
		//check if id was created
		if(id != null) {
			return ResponseEntity.status(HttpStatus.CREATED)
					.header("location", "/masterIncome/incomes/"+id)
					.header("Content-Type", "application/json")
					.body(BaseService.CREATED(
							"Income was created.", "/masterIncome/incomes/"+id));
		}
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
				.body(BaseService.INTERNAL_ERROR("Failed to create the income."));
	}
	
	/**
	 * Create multiple new incomes
	 * @param incomes
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> createMultipleNewIncomes(
							List<Incomes> incomes){
		List<String> responses = new ArrayList<>();
		for(Incomes income: incomes) {
			Long id = null;
			boolean badData = true;
			if(income != null) {
				if(income.getAccount() != null &&
					income.getAmmount() != null &&
					income.getDay() != null &&
					income.getMonth() != null &&
					income.getYear() != null) {
					
					badData = false;
					
					id = incRepo.findById(
							incRepo.save(
								new Incomes(
										income.getMonth(),
										income.getYear(),
										income.getDay(),
										income.getAmmount(),
										income.getAccount())
								).getId()).get().getId();
				}
			}
			if(income == null || badData) {// post was null or bad data
				responses.add(BaseService.BAD_REQUEST("Error in your post body."));
			}
			//check if id was created
			if(id != null) {
				responses.add(BaseService.CREATED(
						"Income was created.", "/masterIncome/incomes/"+id));
			}
		}
		String content ="[";
		for(int i=0;i<responses.size();i++) {
			content += responses.get(i);
			if(i+1!=responses.size()) {
				content+=",";
			}
		}
		content +="]";
		
		return ResponseEntity.status(HttpStatus.CREATED)
				.header("Content-Type", "application/json")
				.body(content);
	}
	
	/**
	 * Get income data by ID
	 * @param id
	 * @param html
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> getIncomeByID(
								Long id,
								Boolean html){
		
		
		List<Incomes> incomes = incRepo.findAllById(Arrays.asList(id));
		
		if(html != null && html == true) {
			String content = "";
			for(Incomes inc : incomes) {
				content += inc.toString();
			}
			return ResponseEntity.status(HttpStatus.OK)
					.body(BaseService.appendBody(Incomes.getTitle(),content));
		}
		if(!incomes.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK)
					.body(incomes.get(0));
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND)
				.body(BaseService.NOT_FOUND("Paystub was not found."));
	}
	
	/**
	 * Patch income data by ID
	 * @param id
	 * @param income
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> updateIncomeByID(
									Long id,
									Incomes income){
		Incomes inc = null;
		inc = incRepo.findById(id).get();
		if(inc != null) {
			
			if(income != null) {
				
				if(income.getAccount() != null) {
					inc.setAccount(income.getAccount());
				}
				
				if(income.getAmmount() != null) {
					inc.setAmmount(income.getAmmount());
				}
				
				if(income.getDay() != null) {
					inc.setDay(income.getDay());
				}
				
				if(income.getMonth() != null) {
					inc.setMonth(income.getMonth());
				}
				
				if(income.getYear() != null) {
					inc.setYear(income.getYear());
				}
			}
			incRepo.save(inc);
		}
		else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.header("Content-Type", "application/json")
					.body(BaseService.BAD_REQUEST("Error in your post body."));
		}
		
		return ResponseEntity.status(HttpStatus.OK)
				.body(inc);
	}
	
	/**
	 * Remove income data by ID
	 * @param id
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> removeIncomeByID(
									Long id){
		
		//check if id exists
		if(incRepo.findById(id).isPresent()) {
			
			//remove id
			incRepo.deleteById(id);
			
			//check if id still exists
			if(incRepo.findById(id).isPresent()) {
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
						.header("Content-Type", "application/json")
						.body(BaseService.INTERNAL_ERROR("Could not remove the income."));
			}
			else {
				return ResponseEntity.status(HttpStatus.NO_CONTENT)
						.body("");
			}
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND)
				.header("Content-Type", "application/json")
				.body(BaseService.NOT_FOUND("Income was not found."));
	}
	
	
}
