package com.hughesportal.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.hughesportal.entity.Expenses;
import com.hughesportal.objects.MasterExpense;
import com.hughesportal.objects.Months;
import com.hughesportal.objects.SingleMasterExpense;
import com.hughesportal.repositories.ExpenseRepository;

/**
 * @author Jacob Hughes
 *
 * Service for the EspenseController, handles
 * all of the business logic
 */
@Service
public class ExpenseService {

	@SuppressWarnings("unused")
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	ExpenseRepository expRepo;
	
	/**
	 * Get Master Expense data
	 * @param year
	 * @param html
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> getMasterExpense(Integer year, Boolean html){
		if(year != null) { //year cannot be null
			
			List<SingleMasterExpense> expenses = expRepo.getSingleExpenseForMasterExpense(year);
			List<MasterExpense> master = new ArrayList<>();
			List<String> categories = new ArrayList<>();
			
			//loop through all expenses
			for(SingleMasterExpense exp : expenses) {
				//category already exists
				if(categories.contains(exp.getCategory())) {
					
					for(int i =0; i<master.size();i++) {
						
						//MasterExpense masterExp = master.get(i);
						if(master.get(i).getCategory().equals(exp.getCategory())) {
							logger.info("Adding ("+exp.getMonth()+", "+exp.getAmmount()+") to master");
							master.get(i).getMonths().setData(exp.getMonth(),exp.getAmmount());
						}
					}
				}
				//category does not exist yet
				else {
					//add the category
					categories.add(exp.getCategory());
					master.add(
						new MasterExpense(
							exp.getCategory(), 
							new Months(exp.getMonth(), exp.getAmmount())));
				}
			}
			
			//return data as html table
			if(html != null && html == true) {
				
				//convert data to string
				String content = "";
				for(MasterExpense masterExp : master) {
					content += masterExp.toString();
				}
				
				return ResponseEntity.status(HttpStatus.OK)
						.body(BaseService.appendBody(MasterExpense.getTitle(),content));
			}
			//return json data
			else {
				return ResponseEntity.status(HttpStatus.OK)
						.body(master);
			}
		}
		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
				.header("Content-Type", "application/json")
				.body(BaseService.BAD_REQUEST("Missing year query parameter."));
	}
	
	/**
	 * Get All Expenses
	 * @param category
	 * @param day
	 * @param gt
	 * @param lt
	 * @param month
	 * @param year
	 * @param html
	 * @param account
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> getAllExpenses(
							String category,
							Integer day,
							Integer gt,
							Integer lt,
							String month,
							Integer year,
							Boolean html,
							Integer account) {
		List<Expenses> expenses = expRepo.findByCategoryAndDayAndMonthAndYearAndAccount(category, day, month, year, account);
		
		if(lt != null) {
			expenses.removeIf(i -> i.getAmmount() > lt);
		}
		
		if(gt != null) {
			expenses.removeIf(i -> i.getAmmount() < gt);
		}
		
		if(html != null && html == true) {
			String content="";
			for(Expenses exp : expenses) {
				content += exp.toString() +"\n";
			}
			return ResponseEntity.status(HttpStatus.OK)
					.body(BaseService.appendBody(Expenses.getTitle(),content));
		}else {
			return ResponseEntity.status(HttpStatus.OK)
					.body(expenses);
		}
	}

	/**
	 * Create a new expense
	 * @param expense
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> createNewExpense(Expenses expense){
		
		Long id = null;
		boolean badData = true;
		if(expense != null) { //check if post is null
			if(expense.getAccount() != null &&
				expense.getAmmount() != null &&
				expense.getCategory() != null &&
				expense.getDay() != null &&
				expense.getMonth() != null &&
				expense.getYear() != null) {
				badData = false;
				id = expRepo.findById(expRepo.save(
						new Expenses(
								expense.getCategory(),
								expense.getMonth(),
								expense.getYear(),
								expense.getDay(),
								expense.getAmmount(),
								expense.getAccount())).getId()).get().getId();
			}
		}
		if(expense == null || badData) {// post was null
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.header("Content-Type", "application/json")
					.body(BaseService.BAD_REQUEST("Error in your post body."));
		}
		
		//check if id was created
		if(id != null) {
			return ResponseEntity.status(HttpStatus.CREATED)
					.header("location", "/expenses/"+id)
					.header("Content-Type", "application/json")
					.body(BaseService.CREATED(
							"Expense was created", 
							"/masterExpense/expenses/"+id));
		}
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("");
	}
	
	/**
	 * Create multiple new expenses
	 * @param expenses
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> createMultipleExpenses(@RequestBody List<Expenses> expenses){
		
		List<String> responses = new ArrayList<>();
		
		for(Expenses expense: expenses) {
			Long id = null;
			boolean badData = true;
			if(expense != null) { //check if post is null
				if(expense.getAccount() != null &&
					expense.getAmmount() != null &&
					expense.getCategory() != null &&
					expense.getDay() != null &&
					expense.getMonth() != null &&
					expense.getYear() != null) {
					badData = false;
					id = expRepo.findById(expRepo.save(
							new Expenses(
									expense.getCategory(),
									expense.getMonth(),
									expense.getYear(),
									expense.getDay(),
									expense.getAmmount(),
									expense.getAccount())).getId()).get().getId();
				}
			}
			if(expense == null || badData) {// post was null
				responses.add(BaseService.BAD_REQUEST("Error in your post body."));
			}
			
			//check if id was created
			if(id != null) {
				responses.add(BaseService.CREATED(
								"Expense was created", 
								"/masterExpense/expenses/"+id));
			}
		}
		String content ="[";
		for(int i=0;i<responses.size();i++) {
			content += responses.get(i);
			if(i+1!=responses.size()) {
				content+=",";
			}
		}
		content +="]";
		
		return ResponseEntity.status(HttpStatus.CREATED)
				.header("Content-Type", "application/json")
				.body(content);
	}
	
	/**
	 * Get expense by ID
	 * @param id
	 * @param html
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> getExpenseByID(
								Long id, Boolean html) {
		List<Expenses> expenses = expRepo.findAllById(Arrays.asList(id));
		
		if(html != null && html == true) {
			String content = "";
			for(Expenses exp : expenses) {
				content += exp.toString();
			}
			return ResponseEntity.status(HttpStatus.OK)
					.body(BaseService.appendBody(Expenses.getTitle(),content));
		}
		if(!expenses.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK)
					.body(expenses.get(0));
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND)
				.header("Content-Type", "application/json")
				.body(BaseService.NOT_FOUND("Paystub was not found."));
	}
	
	/**
	 * Patch the expense by ID
	 * @param id
	 * @param expense
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> updateExpenseByID(
			Long id,
			Expenses expense) {
		
		Expenses exp = null;
		exp = expRepo.findById(id).get();
		if(exp != null) {
			
			if(expense != null) {
				
				if(expense.getAccount() != null) {
					 exp.setAccount(expense.getAccount());
				}
				 
				if(expense.getAmmount() != null) {
					exp.setAmmount(expense.getAmmount());
				}
				
				if(expense.getDay() != null) {
					exp.setDay(expense.getDay());
				}
					
				if(expense.getMonth() != null) {
					exp.setMonth(expense.getMonth());
				}
					
				if(expense.getYear() != null) {
					exp.setYear(expense.getYear());
				}
					
				if(expense.getCategory() != null) {
					exp.setCategory(expense.getCategory());
				}
			}
			expRepo.save(exp);
		}
		else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.header("Content-Type", "application/json")
					.body(BaseService.BAD_REQUEST("Error in your post body."));
		}
		
		return ResponseEntity.status(HttpStatus.OK)
				.body(exp);
	}
	
	/**
	 * Remove an expense by ID
	 * @param id
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> removeExpenseByID(
			Long id) {
		
		//check if id exists
		if(expRepo.findById(id).isPresent()) {
			
			//remove id
			expRepo.deleteById(id);
			
			//check if id still exists
			if(expRepo.findById(id).isPresent()) {
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
						.header("Content-Type", "application/json")
						.body(BaseService.INTERNAL_ERROR("Could not remove the expense."));
			}
			else {
				return ResponseEntity.status(HttpStatus.NO_CONTENT)
						.body("");
			}
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND)
				.header("Content-Type", "application/json")
				.body(BaseService.NOT_FOUND("Expense was not found."));
	}
	
}
