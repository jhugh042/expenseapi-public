package com.hughesportal.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.hughesportal.objects.Accounting;
import com.hughesportal.repositories.ExpenseRepository;
import com.hughesportal.repositories.IncomeRepository;

@Service
public class AccountingService {

	@Autowired
	ExpenseRepository expRepo;
	
	@Autowired
	IncomeRepository incRepo;
	
	/**
	 * Get Accounting book data
	 * @param year
	 * @param month
	 * @param account
	 * @param html
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> getAccountingBook(
									Integer year,
									String month,
									Integer account,
									Boolean html){
		if(year == null || month == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(BaseService.BAD_REQUEST(
							"Must have year and month query parameters."));
		}
		
		List<Accounting> expenses = expRepo.getAllExpenseAmmountsForAccount(year, month, account);
		List<Accounting> incomes = incRepo.getAllIncomeAmmountsForAccount(year, month, account);
		
		expenses.addAll(incomes);
		if(html != null && html == true) {
			String content = "";
			for(Accounting current : expenses) {
				content += current.toString();
			}
			return ResponseEntity.status(HttpStatus.OK)
					.body(BaseService.appendBody(Accounting.getTitle(), content));
		}
		else {
			return ResponseEntity.status(HttpStatus.OK).body(expenses);
		}
	}
}
