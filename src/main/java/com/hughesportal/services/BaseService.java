package com.hughesportal.services;

/**
 * @author Jacob Hughes
 *
 * Base service class holds all the re-useable functions 
 * for the other Service classes
 */
public class BaseService {

	/**
	 * Adds style data to response
	 * @param title
	 * @param main
	 * @return String
	 */
	public static String appendBody(String title,String main) {
		String header = "<style>\r\n" + 
				"table, th, td {\r\n" + 
				"  border: 1px solid black;\r\n" + 
				"  border-collapse: collapse;\r\n" + 
				"}\r\n" + 
				"th, td {\r\n" + 
				"  padding: 5px;\r\n" + 
				"  text-align: left;    \r\n" + 
				"}\r\n" + 
				"</style>";
		
		String startTable ="<table style=\"width:100%\">";
		String endTable ="</table>";
		
		return (header + startTable + title + main + endTable);
	}
	
	/**
	 * Bad request JSON response
	 * @param reason
	 * @return String
	 */
	public static String BAD_REQUEST(String reason) {
		return "{\r\n" + 
				"  \"error\": \"Bad Request.\",\r\n" + 
				"  \"reason\": \""+ reason +"\"\r\n" + 
				"}";
	}
	
	/**
	 * Created JSON response
	 * @param message
	 * @param self
	 * @return String
	 */
	public static String CREATED(String message, String self) {
		return "{" + 
				"  \"message\": \""+ message +"\"," + 
				"  \"self\": \""+self+"\"" + 
				"}";
	}
	
	/**
	 * Not Found JSON response
	 * @param message
	 * @return String
	 */
	public static String NOT_FOUND(String message) {
		return "{\r\n" + 
				"  \"error\": \""+ message +"\"\r\n" + 
				"}";
	}
	
	/**
	 * Internal Error JSON response
	 * @param reason
	 * @return String
	 */
	public static String INTERNAL_ERROR(String reason) {
		return "{\r\n" + 
				"  \"error\": \"Internal Error.\",\r\n" + 
				"  \"reason\": \""+reason+"\"\r\n" + 
				"}";
	}
}
