package com.hughesportal.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.hughesportal.entity.Loanpayment;
import com.hughesportal.repositories.LoanPaymentRepository;

@Service
public class AccountingLoanPaymentService {

	@Autowired
	LoanPaymentRepository payRepo;
	
	/**
	 * Get all loan payments for ID
	 * @param id
	 * @param html
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> getAllLoanPayments(
								Long id,
								Boolean html){
		List<Loanpayment> payments = payRepo.findAllByLoanID(id);
		
		if(html != null && html == true) {
			String content = "";
			for(Loanpayment payment : payments) {
				content += payment.toString() +"\n";
			}
			return ResponseEntity.status(HttpStatus.OK)
					.body(BaseService.appendBody(Loanpayment.getTitle(),content));
		}
		return ResponseEntity.status(HttpStatus.OK)
				.body(payments);
	}
	
	/**
	 * Create a new loan payment for the loan
	 * @param id
	 * @param loanPayment
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> createNewLoanPayment(
								Long id,
								Loanpayment loanPayment){
		Long newID = null;
		boolean badData = true;
		if(loanPayment != null) {
			
			if(loanPayment.getAccount() != null &&
				loanPayment.getAmmount() != null &&
				loanPayment.getMonth() != null &&
				loanPayment.getYear() != null) {
				
				badData = false;
				
				newID = payRepo.findById(payRepo.save(
						new Loanpayment(
							loanPayment.getAccount(),
							loanPayment.getAmmount(),
							loanPayment.getMonth(),
							loanPayment.getYear(),
							id
							)).getId()).get().getId();
			}
		}
		if( loanPayment == null || badData) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.header("Content-Type", "application/json")
					.body(BaseService.BAD_REQUEST("Error in your post body."));
		}
		
		if(newID != null) {
			return ResponseEntity.status(HttpStatus.CREATED)
					.header("location", "/accounting/loans/"+id+"/loanPayment/"+newID)
					.header("Content-Type", "application/json")
					.body(BaseService.CREATED(
							"Loan payment was Created.",
							"/accounting/loans/"+id+"/loanPayment/"+newID));
		}
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("");
	}
	
	/**
	 * Get loan payment with the given ID
	 * @param id
	 * @param pid
	 * @param html
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> getLoanPaymentByID(
			Long id,
			Long pid,
			Boolean html){
		Loanpayment payment = payRepo.findByLoanIDAndLoanPaymentID(id, pid);
		if(html != null && html == true) {
			String content = payment.toString();
			
			return ResponseEntity.status(HttpStatus.OK)
					.body(BaseService.appendBody(Loanpayment.getTitle(),content));
		}
		if(payment != null) {
			return ResponseEntity.status(HttpStatus.OK)
					.body(payment);
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND)
				.body(BaseService.NOT_FOUND("Loan payment was not found."));
	}
	
	public ResponseEntity<?> updateLoanPaymentByID(
			Long id,
			Long pid,
			Loanpayment payment){
		Loanpayment pay = null;
		pay = payRepo.findById(pid).get();
		
		if(pay != null) {
			
			if( payment != null) {
				
				if(payment.getAccount() != null) {
					pay.setAccount(payment.getAccount());
				}
				
				if(payment.getAmmount() != null) {
					pay.setAmmount(payment.getAmmount());
				}
				
				if(payment.getloanId() != null) {
					pay.setloanId(payment.getloanId());
				}
				
				if(payment.getMonth() != null) {
					pay.setMonth(payment.getMonth());
				}
				
				if(payment.getYear() != null) {
					pay.setYear(payment.getYear());
				}
			}
			payRepo.save(pay);
		}
		else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.header("Content-Type", "application/json")
					.body(BaseService.BAD_REQUEST("Error in your post body."));
		}
		
		return ResponseEntity.status(HttpStatus.OK)
				.body(pay);
	}
	
	/**
	 * Deletes the loan with the given ID
	 * @param id
	 * @param pid
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> deleteLoanPaymentByID(
								Long id,
								Long pid){
		
		if(payRepo.findById(pid).isPresent()) {
			payRepo.deleteById(pid);
			
			if(payRepo.findById(pid).isPresent()) {
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
						.header("Content-Type", "application/json")
						.body(BaseService.INTERNAL_ERROR("Could not remove the loan payment."));
			}
			else {
				return ResponseEntity.status(HttpStatus.NO_CONTENT)
						.body("");
			}
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND)
				.header("Content-Type", "application/json")
				.body(BaseService.NOT_FOUND("Loan payment was not found."));
		
	}
}
