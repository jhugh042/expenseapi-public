package com.hughesportal.services;

import java.util.Arrays;
import java.util.List;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import com.hughesportal.entity.Incomes;
import com.hughesportal.entity.Paystub;
import com.hughesportal.repositories.IncomeRepository;
import com.hughesportal.repositories.PaystubRepository;
import org.slf4j.Logger;

/** 
 * @author Jacob Hughes
 *
 * Service for the AccountingController, handles
 * all of the business logic
 */
@Service
public class AccountingPaystubService {

	@SuppressWarnings("unused")
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	IncomeRepository incRepo;
	
	@Autowired
	PaystubRepository payRepo;
	
	/**
	 * Get all paystubs
	 * @param year
	 * @param html
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> getPayStubs(Integer year,Boolean html){
		if(year != null) {
			List<Paystub> paystubs = payRepo.findAllByYear(year);
			
			if(html != null && html == true) {
				String content = "";
				Double grossYTD = 0.00, fedYTD = 0.00,stateYTD = 0.00,
				ssnYTD = 0.00,medicareYTD = 0.00,retirementYTD = 0.00,
				netYTD = 0.00;
				for(Paystub pay: paystubs) {
					grossYTD += pay.getGrossPay();
					fedYTD += pay.getFedTax();
					stateYTD += pay.getStateTax();
					ssnYTD += pay.getSsnTax();
					medicareYTD += pay.getMedicare();
					retirementYTD += pay.getRetirement();
					netYTD += pay.getNetPay();
					content += pay.toStringYTD(grossYTD,fedYTD,stateYTD,
							ssnYTD,medicareYTD,retirementYTD,netYTD);
				}
				return ResponseEntity.status(HttpStatus.OK)
						.body(BaseService.appendBody(Paystub.getTitleYTD(),content));
			}
			return ResponseEntity.status(HttpStatus.OK)
					.body(paystubs);
		}
		return ResponseEntity.status(HttpStatus.OK)
				.body(BaseService.BAD_REQUEST("Year is a required query parameter."));
	}
	
	/**
	 * Create a new paystub
	 * @param paystub
	 * @param createIncome
	 * @param account
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> createPayStub(
			Paystub paystub, Boolean createIncome, Integer account){
		Long id = null;
		boolean badData = true;
		if(paystub != null) { //check if post is null
			logger.info("PAYSTUB DATA:"+paystub.toString());
			if(paystub.getFedTax() != null &&
				paystub.getGrossPay() != null &&
				paystub.getMedicare() != null &&
				paystub.getNetPay() != null &&
				paystub.getSsnTax() != null &&
				paystub.getStateTax() != null &&
				paystub.getDay() != null &&
				paystub.getMonth() != null &&
				paystub.getYear() != null) {
				
				badData = false;
				
				Double retirement;
				if(paystub.getRetirement() != null) {
					retirement = paystub.getRetirement();
				}else {
					retirement = 0.00;
				}
				
				if(createIncome != null && createIncome == true) {
					if(account != null) {
						incRepo.save(
								new Incomes(
										paystub.getMonth(),
										paystub.getYear(),
										paystub.getDay(),
										paystub.getNetPay(),
										account));
					}
					else {
						return ResponseEntity.status(HttpStatus.BAD_REQUEST)
								.header("Content-Type", "application/json")
								.body(BaseService.BAD_REQUEST(
										"Account cannot be empty if createIncome is true."));
					}
				}
				
				id = payRepo.findById(payRepo.save(
						new Paystub(
								paystub.getMonth(),
								paystub.getYear(),
								paystub.getDay(),
								paystub.getGrossPay(),
								paystub.getFedTax(),
								paystub.getStateTax(),
								paystub.getSsnTax(),
								paystub.getMedicare(),
								retirement,
								paystub.getNetPay())).getId()).get().getId();
			}
		}
		if(paystub == null || badData) {// post was null or bad data
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.header("Content-Type", "application/json")
					.body(BaseService.BAD_REQUEST("Error in your post body."));
		}
		
		//check if id was created
		if(id != null) {
			return ResponseEntity.status(HttpStatus.CREATED)
					.header("location", "/accounting/paystubs/"+id)
					.header("Content-Type", "application/json")
					.body(BaseService.CREATED(
							"Paystub was Created.",
							"/accounting/paystubs/"+id));
		}
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("");   
	}
	
	 /**
	  * get a paystub by ID
	  * @param id
	  * @param html
	  * @return ResponseEntity
	  */
	public ResponseEntity<?> getPayStubByID(Long id, Boolean html){
		List<Paystub> paystubs = payRepo.findAllById(Arrays.asList(id));
		if(html != null && html == true) {
			String content = "";
			for(Paystub pay: paystubs) {
				content += pay.toString();
			}
			return ResponseEntity.status(HttpStatus.OK)
					.body(BaseService.appendBody(Paystub.getTitle(),content));
		}
		if(!paystubs.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK)
					.body(paystubs.get(0));
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND)
				.body(BaseService.NOT_FOUND("Paystub was not found."));
	}
	
	/**
	 * Patch the paystub with the ID
	 * @param id
	 * @param paystub
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> updatePayStubByID(
									Long id, 
									Paystub paystub){
		Paystub pay = null;
		pay = payRepo.findById(id).get();
		if( pay != null) {
			
			if(paystub != null) {
				
				//update day
				if(paystub.getDay() != null) {
					pay.setDay(paystub.getDay());
				}
				
				//update fed tax
				if(paystub.getFedTax() != null) {
					pay.setFedTax(paystub.getFedTax());
				}
				
				//update gross pay
				if(paystub.getGrossPay() != null) {
					pay.setGrossPay(paystub.getGrossPay());
				}
				
				//update mediacre
				if(paystub.getMedicare() != null) {
					pay.setMedicare(paystub.getMedicare());
				}
				
				//update month
				if(paystub.getMonth() != null) {
					pay.setMonth(paystub.getMonth());
				}
				
				//update net pay
				if(paystub.getNetPay() != null) {
					pay.setNetPay(paystub.getNetPay());
				}
				
				//update Retirement
				if(paystub.getRetirement() != null) {
					pay.setRetirement(paystub.getRetirement());
				}
				
				//update SSN
				if(paystub.getSsnTax() != null) {
					pay.setSsnTax(paystub.getSsnTax());
				}
				
				//update stateTax
				if(paystub.getStateTax() != null) {
					pay.setStateTax(paystub.getStateTax());
				}
				
				//update year
				if(paystub.getYear() != null) {
					pay.setYear(paystub.getYear());
				}
			}
			payRepo.save(pay);
		}
		else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.header("Content-Type", "application/json")
					.body(BaseService.BAD_REQUEST("Error in your post body."));
		}
		
		return ResponseEntity.status(HttpStatus.OK)
				.body(pay);
	}
	
	/**
	 * Remove paystub with ID
	 * @param id
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> removePayStubByID(
			@PathVariable Long id){
		
		if(payRepo.findById(id).isPresent()) {
			payRepo.deleteById(id);
			
			if(payRepo.findById(id).isPresent()) {
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
						.header("Content-Type", "application/json")
						.body(BaseService.INTERNAL_ERROR("Could not remove the paystub."));
			}
			else {
				return ResponseEntity.status(HttpStatus.NO_CONTENT)
						.body("");
			}
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND)
				.header("Content-Type", "application/json")
				.body(BaseService.NOT_FOUND("Paystub was not found."));
	}
}
