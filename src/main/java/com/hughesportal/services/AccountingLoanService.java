package com.hughesportal.services;

import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.TextStyle;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.hughesportal.entity.Loanpayment;
import com.hughesportal.entity.Loans;
import com.hughesportal.objects.LoanEstimate;
import com.hughesportal.repositories.LoanPaymentRepository;
import com.hughesportal.repositories.LoanRepository;

@Service
public class AccountingLoanService {

	@SuppressWarnings("unused")
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	LoanRepository loanRepo;
	
	@Autowired
	LoanPaymentRepository loanPaymentRepo;
	
	/**
	 * Get all loans
	 * @param html
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> getAllLoans(Boolean html){
		List<Loans> loans = loanRepo.findAll();
		
		if(html != null && html == true) {
			String content = "";
			for(Loans loan : loans) {
				content += loan.toString()+"\n";
			}
			return ResponseEntity.status(HttpStatus.OK)
					.body(BaseService.appendBody(Loans.getTitle(),content));
		}
		return ResponseEntity.status(HttpStatus.OK)
				.body(loans);
	}
	
	/**
	 * Create a new loan
	 * @param loan
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> createNewLoan(Loans loan){
		Long id = null;
		boolean badData=true;
		if(loan != null) {
			if(loan.getAmmount() != null &&
				loan.getLoanEndMonth() != null &&
				loan.getLoanEndYear() != null &&
				loan.getLoanStartMonth() != null &&
				loan.getLoanStartYear() != null &&
				loan.getName() != null &&
				loan.getInterest() != null) {
				
				badData = false;
				
				id = loanRepo.findById(loanRepo.save(
						new Loans(
							loan.getName(),
							loan.getAmmount(),
							loan.getLoanStartMonth(),
							loan.getLoanStartYear(),
							loan.getLoanEndMonth(),
							loan.getLoanEndYear(),
							loan.getInterest()
								)).getId()).get().getId();
			}
		}
		if( loan == null || badData) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.header("Content-Type", "application/json")
					.body(BaseService.BAD_REQUEST("Error in your post body."));
		}
		
		if(id != null) {
			return ResponseEntity.status(HttpStatus.CREATED)
					.header("location", "/accounting/loans/"+id)
					.header("Content-Type", "application/json")
					.body(BaseService.CREATED(
							"Loan was Created.",
							"/accounting/loans/"+id));
		}
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("");  
	}
	
	/**
	 * Get Loan with the given ID
	 * @param id
	 * @param html
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> getLoanByID(Long id, Boolean html){
		List<Loans> loans = loanRepo.findAllById(Arrays.asList(id));
		if(html != null && html == true) {
			String content = "";
			for(Loans loan : loans) {
				content += loan.toString() +"\n";
			}
			return ResponseEntity.status(HttpStatus.OK)
					.body(BaseService.appendBody(Loans.getTitle(),content));
		}
		if(!loans.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK)
					.body(loans.get(0));
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND)
				.body(BaseService.NOT_FOUND("Loan was not found."));
	}
	
	/**
	 * Update the loan with the given ID
	 * @param id
	 * @param loans
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> updateLoanByID(
			Long id,
			Loans loans){
		Loans loan = null;
		loan = loanRepo.findById(id).get();
		
		if(loan != null) {
			
			if(loans != null) {
				
				if(loans.getAmmount() != null) {
					loan.setAmmount(loans.getAmmount());
				}
				
				if(loans.getLoanStartMonth() != null) {
					loan.setLoanStartMonth(loans.getLoanStartMonth());
				}
				
				if(loans.getLoanStartYear() != null) {
					loan.setLoanStartYear(loans.getLoanStartYear());
				}
				
				if(loans.getLoanEndMonth() != null) {
					loan.setLoanEndMonth(loans.getLoanEndMonth());
				}
				
				if(loans.getLoanEndYear() != null) {
					loan.setLoanEndYear(loans.getLoanEndYear());
				}
				
				if(loans.getName() != null) {
					loan.setName(loans.getName());
				}
				
				if(loans.getInterest() != null) {
					loan.setInterest(loans.getInterest());
				}
			}
			loanRepo.save(loan);
		}
		else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.header("Content-Type", "application/json")
					.body(BaseService.BAD_REQUEST("Error in your post body."));
		}
		
		return ResponseEntity.status(HttpStatus.OK)
				.body(loan);
	}
	
	/**
	 * Delete the loan with the given ID
	 * @param id
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> deleteLoanByID(
									Long id){
		
		if(loanRepo.findById(id).isPresent()) {
			loanRepo.deleteById(id);
			
			if(loanRepo.findById(id).isPresent()) {
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
						.header("Content-Type", "application/json")
						.body(BaseService.INTERNAL_ERROR("Could not remove the loan."));
			}
			else {
				return ResponseEntity.status(HttpStatus.NO_CONTENT)
						.body("");
			}
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND)
				.header("Content-Type", "application/json")
				.body(BaseService.NOT_FOUND("Loan was not found."));
	}
	
	public ResponseEntity<?> getLoanEstimate(Long id,Boolean html,Integer paymentAmmt){
		
		//get the loan
		Loans loan = loanRepo.findById(id).get();
		if(loan != null) {
			//create the array
			List<LoanEstimate> estimates = new ArrayList<>();
			List<Loanpayment> loanPayments = loanPaymentRepo.findAllByLoanID(id);
			
			//make it easy to compare and increment month
			Month startMonth = parseMonth(loan.getLoanStartMonth(), TextStyle.FULL, Locale.US);
			Month endMonth = parseMonth(loan.getLoanEndMonth(), TextStyle.FULL, Locale.US);
			
			//setup the data for increments
			int currentYear = loan.getLoanStartYear();
			double interestYTD =0.00;
			double principalYTD =0.00;
			
			Long monthsElapsed = getMonthsBetween(
									loan.getLoanStartYear(), startMonth.getValue(),
									loan.getLoanEndYear(), endMonth.getValue());
			
			//calculate payment info
			double i = (loan.getInterest() / 100) /12;
			double discountFactor = 
					(Math.pow(1+i, monthsElapsed)-1)/
					(Math.pow(1+i, monthsElapsed)*i);
			double payment, originalPayment;
			if(paymentAmmt != null) {
				payment = paymentAmmt;
			}
			else {
				payment = loan.getAmmount() / discountFactor;
			}
			originalPayment = payment;
			
			logger.info("Start:"+startMonth+"/"+loan.getLoanStartYear()
						+"  End:"+endMonth+"/"+loan.getLoanEndYear()
						+"  LoanAmmount:"+loan.getAmmount());
			
			//while not done paying loan
			while(!(startMonth == endMonth &&
					currentYear == loan.getLoanEndYear())
					&& loan.getAmmount() > 0) {
				
				Double tempPay= null;
				//check if a loan payment already exists
				for(Loanpayment loanPay : loanPayments) {
					if(loanPay.getMonth().equals(startMonth.getDisplayName(TextStyle.FULL, Locale.US))
						&& loanPay.getYear().equals(currentYear)) {
						
						tempPay = loanPay.getAmmount();
						payment = tempPay;
					}
				}
				
				//calculate interest and principal
				double interest = i * loan.getAmmount();
				double principal = payment - interest;
				if(principal < 0) {
					principal = 0;
				}
				else if (principal > loan.getAmmount()) {
					principal = loan.getAmmount();
					payment = interest + principal;
				}
				
				//add them to YTD
				principalYTD += principal;
				interestYTD += interest;
				
				//update loan ammount
				loan.setAmmount(loan.getAmmount() - principal);
				
				//payment already exists
				if(tempPay != null) {
					//create new loan estimate
					estimates.add(new LoanEstimate(
							startMonth.getDisplayName(TextStyle.FULL, Locale.US)+"/"+currentYear,
							payment,interest,interestYTD,principal, principalYTD, loan.getAmmount()));
				}else {
					//create new loan estimate
					estimates.add(new LoanEstimate(
							startMonth.getDisplayName(TextStyle.FULL, Locale.US)+"/"+currentYear,
							payment,interest,interestYTD,principal, principalYTD, loan.getAmmount()));
				
				}
				
				//increment month and year
				startMonth = startMonth.plus(1);
				if(startMonth.getValue()==1) {
					currentYear++;
				}
				payment = originalPayment;
			}
			//return data in html
			if(html != null && html == true) {
				String content = "";
				for(LoanEstimate est : estimates) {
					content += est.toString() +"\n";
				}
				return ResponseEntity.status(HttpStatus.OK)
						.body(BaseService.appendBody(LoanEstimate.getTitle(),content));
			}
			//return json data
			return ResponseEntity.status(HttpStatus.OK)
					.body(estimates);
		}
		else {
			//id not found
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.header("Content-Type", "application/json")
					.body(BaseService.NOT_FOUND("Loan was not found."));
		}
	}
	
	private Month parseMonth(CharSequence text, TextStyle style, Locale locale) {
	    DateTimeFormatter fmt = new DateTimeFormatterBuilder()
	            .appendText(ChronoField.MONTH_OF_YEAR, style)
	            .toFormatter(locale);
	    return Month.from(fmt.parse(text));
	}
	
	private Long getMonthsBetween(int startYear, int startMonth, int endYear, int endMonth) {
		String start,end;
		if(startMonth <10) {
			start = startYear+"-0"+startMonth+"-01";
		}else {
			start = startYear+"-"+startMonth+"-01";
		}
		
		if(endMonth < 10) {
			end = endYear+"-0"+endMonth+"-01";
		}
		else {
			end = endYear+"-"+endMonth+"-01";
		}
		long monthsBetween = ChronoUnit.MONTHS.between(
		        LocalDate.parse(start).withDayOfMonth(1),
		        LocalDate.parse(end).withDayOfMonth(1));
		return monthsBetween;
	}
	
	
}
