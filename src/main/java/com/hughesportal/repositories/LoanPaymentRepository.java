package com.hughesportal.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hughesportal.entity.Loanpayment;

@Repository
public interface LoanPaymentRepository extends JpaRepository<Loanpayment,Long>{

	@Query("SELECT c FROM "
		 + "Loanpayment c "
		 + "WHERE c.loanId = :id ")
	public List<Loanpayment> findAllByLoanID(Long id);
	
	
	@Query("SELECT c "
		 + "FROM Loanpayment c "
		 + "WHERE c.loanId = :id "
		 + "AND c.id = :pid ")
	public Loanpayment findByLoanIDAndLoanPaymentID(Long id, Long pid);
	
	
}
