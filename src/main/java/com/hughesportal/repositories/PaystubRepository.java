package com.hughesportal.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hughesportal.entity.Paystub;

/**
 * @author Jacob Hughes
 *
 * Database queries interface for paystub table
 */
@Repository
public interface PaystubRepository extends JpaRepository<Paystub,Long> {

	@Query("SELECT c "
		+ "FROM Paystub c "
		+ "WHERE c.year = :year ")
	List<Paystub> findAllByYear(Integer year);
}
