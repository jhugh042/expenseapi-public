package com.hughesportal.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hughesportal.entity.Incomes;
import com.hughesportal.objects.Accounting;
import com.hughesportal.objects.SingleMasterIncome;

/**
 * @author Jacob Hughes
 *
 * Database queries interface for Income table
 */
@Repository
public interface IncomeRepository extends JpaRepository<Incomes,Long> {

	@Query("SELECT c FROM Incomes c "
			+ "WHERE (:account is null or c.account = :account) "
			+ "and (:day is null or c.day = :day) "
			+ "and (:month is null or c.month = :month) "
			+ "and (:year is null or c.year = :year)")
	List<Incomes> findByDayAndMonthAndYearAndAccount(
							Integer day,
							String month,
							Integer year,
							Integer account);
	
	@Query("Select "
			+ "new com.hughesportal.objects.SingleMasterIncome( "
				+ "SUM(c.ammount), c.account, c.month) " + 
			"FROM Incomes c " + 
			"WHERE (c.year = :year ) " + 
			"GROUP BY c.account, c.month "+
			"ORDER BY c.account")
	List<SingleMasterIncome> getSingleIncomeForMasterIncome(Integer year);
	
	@Query("SELECT "
				+ "new com.hughesportal.objects.Accounting( "
				+ "c.account, c.ammount, false) "
			+ "FROM Incomes c "
			+ "WHERE (:account is null or c.account = :account) "
			+ "and (c.year = :year) "
			+ "and (c.month = :month)")
	List<Accounting> getAllIncomeAmmountsForAccount(Integer year, String month, Integer account);
		
}
