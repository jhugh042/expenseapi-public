package com.hughesportal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hughesportal.entity.Loans;

@Repository
public interface LoanRepository extends JpaRepository<Loans,Long>{

}
