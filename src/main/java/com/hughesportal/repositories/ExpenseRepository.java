package com.hughesportal.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hughesportal.entity.Expenses;
import com.hughesportal.objects.Accounting;
import com.hughesportal.objects.SingleMasterExpense;

/**
 * @author Jacob Hughes
 *
 * Database queries interface for Expense table
 */
@Repository
public interface ExpenseRepository extends JpaRepository<Expenses, Long>{
	
	@Query("SELECT c FROM Expenses c "
			+ "WHERE (:category is null or c.category = :category) "
			+ "and (:account is null or c.account = :account) "
			+ "and (:day is null or c.day = :day) "
			+ "and (:month is null or c.month = :month) "
			+ "and (:year is null or c.year = :year)")
	List<Expenses> findByCategoryAndDayAndMonthAndYearAndAccount(String category,
							Integer day,
							String month,
							Integer year,
							Integer account);
	
	@Query("Select "
			+ "new com.hughesportal.objects.SingleMasterExpense( "
				+ "SUM(c.ammount), c.category, c.month) " + 
			"FROM Expenses c " + 
			"WHERE (c.year = :year ) " + 
			"GROUP BY c.category, c.month "+
			"ORDER BY c.category")
	List<SingleMasterExpense> getSingleExpenseForMasterExpense(Integer year);
	
	@Query("SELECT "
			+ "new com.hughesportal.objects.Accounting("
			+ "c.account, c.ammount, true) "
		+ "FROM Expenses c "
		+ "WHERE (:account is null or c.account = :account) "
		+ "and (c.year = :year) "
		+ "and (c.month = :month)")
	List<Accounting> getAllExpenseAmmountsForAccount(Integer year, String month, Integer account);
	
}
