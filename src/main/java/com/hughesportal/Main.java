package com.hughesportal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.hughesportal.repositories.ExpenseRepository;

/**
 * @author Jacob Hughes
 *
 * Main class - boots up the spring application
 */
@SpringBootApplication
public class Main implements CommandLineRunner{

	@SuppressWarnings("unused")
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	ExpenseRepository expRepo;
	
	public static void main(String[] args) {
		SpringApplication.run(Main.class,args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		//logger.info("=================TESTING1234================================");
		//expRepo.save(new Expenses("Food","May",2019,15,2.13,1356));
		//expRepo.save(new Expenses("Food","May",2019,15,2.13,1355));
		//expRepo.save(new Expenses("Food","May",2019,15,5.00,1356));
		//expRepo.save(new Expenses("Food","May",2019,15,10.00,1355));
		
	}

}
