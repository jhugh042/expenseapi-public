package com.hughesportal.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Jacob Hughes
 *
 * Databese entity for creating and using paystub table
 */
@Entity
public class Paystub {

	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private String month;
	private Integer year;
	private Integer day;
	private Double grossPay;
	private Double fedTax;
	private Double stateTax;
	private Double ssnTax;
	private Double medicare;
	private Double retirement;
	private Double netPay;
	
	Paystub(){}

	/**
	 * @param id
	 * @param month
	 * @param year
	 * @param day
	 * @param grossPay
	 * @param fedTax
	 * @param stateTax
	 * @param ssnTax
	 * @param medicare
	 * @param retirement
	 * @param netPay
	 */
	public Paystub(String month, Integer year, Integer day, Double grossPay, Double fedTax, Double stateTax,
			Double ssnTax, Double medicare, Double retirement, Double netPay) {
		this.month = month;
		this.year = year;
		this.day = day;
		this.grossPay = grossPay;
		this.fedTax = fedTax;
		this.stateTax = stateTax;
		this.ssnTax = ssnTax;
		this.medicare = medicare;
		this.retirement = retirement;
		this.netPay = netPay;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the month
	 */
	public String getMonth() {
		return month;
	}

	/**
	 * @param month the month to set
	 */
	public void setMonth(String month) {
		this.month = month;
	}

	/**
	 * @return the year
	 */
	public Integer getYear() {
		return year;
	}

	/**
	 * @param year the year to set
	 */
	public void setYear(Integer year) {
		this.year = year;
	}

	/**
	 * @return the day
	 */
	public Integer getDay() {
		return day;
	}

	/**
	 * @param day the day to set
	 */
	public void setDay(Integer day) {
		this.day = day;
	}

	/**
	 * @return the grossPay
	 */
	public Double getGrossPay() {
		return grossPay;
	}

	/**
	 * @param grossPay the grossPay to set
	 */
	public void setGrossPay(Double grossPay) {
		this.grossPay = grossPay;
	}

	/**
	 * @return the fedTax
	 */
	public Double getFedTax() {
		return fedTax;
	}

	/**
	 * @param fedTax the fedTax to set
	 */
	public void setFedTax(Double fedTax) {
		this.fedTax = fedTax;
	}

	/**
	 * @return the stateTax
	 */
	public Double getStateTax() {
		return stateTax;
	}

	/**
	 * @param stateTax the stateTax to set
	 */
	public void setStateTax(Double stateTax) {
		this.stateTax = stateTax;
	}

	/**
	 * @return the ssnTax
	 */
	public Double getSsnTax() {
		return ssnTax;
	}

	/**
	 * @param ssnTax the ssnTax to set
	 */
	public void setSsnTax(Double ssnTax) {
		this.ssnTax = ssnTax;
	}

	/**
	 * @return the medicare
	 */
	public Double getMedicare() {
		return medicare;
	}

	/**
	 * @param medicare the medicare to set
	 */
	public void setMedicare(Double medicare) {
		this.medicare = medicare;
	}

	/**
	 * @return the retirement
	 */
	public Double getRetirement() {
		return retirement;
	}

	/**
	 * @param retirement the retirement to set
	 */
	public void setRetirement(Double retirement) {
		this.retirement = retirement;
	}

	/**
	 * @return the netPay
	 */
	public Double getNetPay() {
		return netPay;
	}

	/**
	 * @param netPay the netPay to set
	 */
	public void setNetPay(Double netPay) {
		this.netPay = netPay;
	}

	public static String getTitle() {
		return "<tr>"
				+ "<th>ID</th>"
				+ "<th>Gross Pay</th>"
				+ "<th>Federal Tax</th>"
				+ "<th>State Tax</th>"
				+ "<th>SSN</th>"
				+ "<th>Medicare</th>"
				+ "<th>Retirement</th>"
				+ "<th>Net Pay</th>"
				+ "<th>Year</th>"
				+ "<th>Month</th>"
				+ "<th>Day</th>"
				+ "</tr>";
		
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("<tr>"
							+ "<td>%d</td>"
							+ "<td>%.2f</td>"
							+ "<td>%.2f</td>"
							+ "<td>%.2f</td>"
							+ "<td>%.2f</td>"
							+ "<td>%.2f</td>"
							+ "<td>%.2f</td>"
							+ "<td>%.2f</td>"
							+ "<td>%d</td>"
							+ "<td>%s</td>"
							+ "<td>%d</td>"
							+ "</tr>",
				id, grossPay, fedTax, stateTax,
				ssnTax, medicare,retirement,netPay,
				year,month,day);
	}
	
	public static String getTitleYTD() {
		return "<tr>"
				+ "<th>ID</th>"
				+ "<th>Gross</th>"
				+ "<th>Gross YTD</th>"
				+ "<th>Federal Tax</th>"
				+ "<th>Federal YTD</th>"
				+ "<th>State Tax</th>"
				+ "<th>State YTD</th>"
				+ "<th>SSN</th>"
				+ "<th>SSN YTD</th>"
				+ "<th>Medicare</th>"
				+ "<th>Medicare YTD</th>"
				+ "<th>Retirement</th>"
				+ "<th>Retirement YTD</th>"
				+ "<th>Net</th>"
				+ "<th>Net YTD</th>"
				+ "<th>Date</th>"
				+ "</tr>";
		
	}
	
	public String toStringYTD(Double grossYTD,Double fedYTD,Double stateYTD,
			Double ssnYTD, Double mediYTD, Double retYTD, Double netYTD) {
		return String.format("<tr>"
							+ "<td>%d</td>"
							+ "<td>%.2f</td>"
							+ "<td>%.2f</td>"
							+ "<td>%.2f</td>"
							+ "<td>%.2f</td>"
							+ "<td>%.2f</td>"
							+ "<td>%.2f</td>"
							+ "<td>%.2f</td>"
							+ "<td>%.2f</td>"
							+ "<td>%.2f</td>"
							+ "<td>%.2f</td>"
							+ "<td>%.2f</td>"
							+ "<td>%.2f</td>"
							+ "<td>%.2f</td>"
							+ "<td>%.2f</td>"
							+ "<td>%s,%d</td>"
							+ "</tr>",
				id, grossPay,grossYTD, fedTax,fedYTD, stateTax,stateYTD,
				ssnTax,ssnYTD, medicare,mediYTD,retirement,retYTD,netPay,netYTD,
				month,day);
	}
	
}
