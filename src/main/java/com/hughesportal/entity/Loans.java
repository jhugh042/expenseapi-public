package com.hughesportal.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Loans {

	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private String name;
	private Double ammount;
	private Double interest;
	private String loanStartMonth;
	private Integer loanStartYear;
	private String loanEndMonth;
	private Integer loanEndYear;
	
	protected Loans() {}
	
	/**
	 * @param name
	 * @param ammount
	 * @param loanStartMonth
	 * @param loanStartYear
	 * @param loanEndMonth
	 * @param loanEndYear
	 */
	public Loans(String name, Double ammount, String loanStartMonth, Integer loanStartYear, String loanEndMonth,
			Integer loanEndYear, Double interest) {
		this.name = name;
		this.ammount = ammount;
		this.loanStartMonth = loanStartMonth;
		this.loanStartYear = loanStartYear;
		this.loanEndMonth = loanEndMonth;
		this.loanEndYear = loanEndYear;
		this.interest = interest;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the ammount
	 */
	public Double getAmmount() {
		return ammount;
	}

	/**
	 * @param ammount the ammount to set
	 */
	public void setAmmount(Double ammount) {
		this.ammount = ammount;
	}

	/**
	 * @return the loanStartMonth
	 */
	public String getLoanStartMonth() {
		return loanStartMonth;
	}

	/**
	 * @param loanStartMonth the loanStartMonth to set
	 */
	public void setLoanStartMonth(String loanStartMonth) {
		this.loanStartMonth = loanStartMonth;
	}

	/**
	 * @return the loanStartYear
	 */
	public Integer getLoanStartYear() {
		return loanStartYear;
	}

	/**
	 * @param loanStartYear the loanStartYear to set
	 */
	public void setLoanStartYear(Integer loanStartYear) {
		this.loanStartYear = loanStartYear;
	}

	/**
	 * @return the loanEndMonth
	 */
	public String getLoanEndMonth() {
		return loanEndMonth;
	}

	/**
	 * @param loanEndMonth the loanEndMonth to set
	 */
	public void setLoanEndMonth(String loanEndMonth) {
		this.loanEndMonth = loanEndMonth;
	}

	/**
	 * @return the loanEndYear
	 */
	public Integer getLoanEndYear() {
		return loanEndYear;
	}

	/**
	 * @param loanEndYear the loanEndYear to set
	 */
	public void setLoanEndYear(Integer loanEndYear) {
		this.loanEndYear = loanEndYear;
	}
	
	
	
	/**
	 * @return the interest
	 */
	public Double getInterest() {
		return interest;
	}

	/**
	 * @param interest the interest to set
	 */
	public void setInterest(Double interest) {
		this.interest = interest;
	}

	public static String getTitle() {
		return "<tr>"
			+ "<th>ID</th>"
			+ "<th>Name</th>"
			+ "<th>Ammount</th>"
			+ "<th>Interest</th>"
			+ "<th>Loan Start</th>"
			+ "<th>Loan End</th>"
			+ "</tr>";
	}
	
	@Override
	public String toString() {
		return String.format("<tr>"
							+ "<td>%d</td>"
							+ "<td>%s</td>"
							+ "<td>%.2f</td>"
							+ "<td>%.2f</td>"
							+ "<td>%s</td>"
							+ "<td>%s</td>"
							+ "</tr>",
					id,name,ammount,interest,
					loanStartMonth+"/"+loanStartYear,
					loanEndMonth+"/"+loanEndYear);
	}
	
	
}
