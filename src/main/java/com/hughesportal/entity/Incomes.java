package com.hughesportal.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Jacob Hughes
 *
 * Databese entity for creating and using income table
 */
@Entity
public class Incomes {


	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private Integer account;
	private String month;
	private Integer year;
	private Integer day;
	private Double ammount;

	
	protected Incomes() {}
	
	
	/**
	 * @param month
	 * @param year
	 * @param day
	 * @param ammount
	 * @param account
	 */
	public Incomes(String month, Integer year, Integer day, Double ammount, Integer account) {
		this.month = month;
		this.year = year;
		this.day = day;
		this.ammount = ammount;
		this.account = account;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 * @return the month
	 */
	public String getMonth() {
		return month;
	}

	/**
	 * @param month the month to set
	 */
	public void setMonth(String month) {
		this.month = month;
	}

	/**
	 * @return the year
	 */
	public Integer getYear() {
		return year;
	}

	/**
	 * @param year the year to set
	 */
	public void setYear(Integer year) {
		this.year = year;
	}

	/**
	 * @return the day
	 */
	public Integer getDay() {
		return day;
	}

	/**
	 * @param day the day to set
	 */
	public void setDay(Integer day) {
		this.day = day;
	}

	/**
	 * @return the ammount
	 */
	public Double getAmmount() {
		return ammount;
	}

	/**
	 * @param ammount the ammount to set
	 */
	public void setAmmount(Double ammount) {
		this.ammount = ammount;
	}
	
	/**
	 * @return the account
	 */
	public Integer getAccount() {
		return account;
	}

	/**
	 * @param account the account to set
	 */
	public void setAccount(Integer account) {
		this.account = account;
	}

	
	public static String getTitle() {
		return "<tr>"
			+ "<th>ID</th>"
			+ "<th>Account</th>"
			+ "<th>Ammount</th>"
			+ "<th>Year</th>"
			+ "<th>Month</th>"
			+ "<th>Day</th>"
			+ "</tr>";
	}
	
	@Override
	public String toString() {
		return String.format("<tr>"
							+ "<td>%d</td>"
							+ "<td>%d</td>"
							+ "<td>$%.2f</td>"
							+ "<td>%d</td>"
							+ "<td>%s</td>"
							+ "<td>%d</td>"
							+ "</tr>",
				id,account,ammount,year,month,day);
	}
}
