package com.hughesportal.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Loanpayment {

	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private Integer account;
	private Double ammount;
	private String month;
	private Integer year;
	private Long loanId;
	
	protected Loanpayment(){}
	
	/**
	 * @param account
	 * @param ammount
	 * @param month
	 * @param year
	 */
	public Loanpayment(Integer account, Double ammount, String month, Integer year, Long loanId) {
		this.account = account;
		this.ammount = ammount;
		this.month = month;
		this.year = year;
		this.loanId = loanId;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the account
	 */
	public Integer getAccount() {
		return account;
	}

	/**
	 * @param account the account to set
	 */
	public void setAccount(Integer account) {
		this.account = account;
	}

	/**
	 * @return the ammount
	 */
	public Double getAmmount() {
		return ammount;
	}

	/**
	 * @param ammount the ammount to set
	 */
	public void setAmmount(Double ammount) {
		this.ammount = ammount;
	}

	/**
	 * @return the month
	 */
	public String getMonth() {
		return month;
	}

	/**
	 * @param month the month to set
	 */
	public void setMonth(String month) {
		this.month = month;
	}

	/**
	 * @return the year
	 */
	public Integer getYear() {
		return year;
	}

	/**
	 * @param year the year to set
	 */
	public void setYear(Integer year) {
		this.year = year;
	}

	public Long getloanId() {
		return loanId;
	}

	public void setloanId(Long loanId) {
		this.loanId = loanId;
	}
	
	/*
	 * 	private Long id;
	
	private Integer account;
	private Double ammount;
	private String month;
	private Integer year;
	private Long loanId;
	 */
	public static String getTitle() {
		return "<tr>"
			+ "<th>ID</th>"
			+ "<th>Loan ID</th>"
			+ "<th>Account</th>"
			+ "<th>Ammount</th>"
			+ "<th>Date</th>"
			+ "</tr>";
	}

	@Override
	public String toString() {
		return String.format(
				"<tr>"
			  + "<td>%d</td>"
			  + "<td>%d</td>"
			  + "<td>%d</td>"
			  + "<td>%.2f</td>"
			  + "<td>%s</td>"
			  + "</tr>",
			  id,loanId,account,ammount,month+"/"+year);
	}
	
	
	
}
