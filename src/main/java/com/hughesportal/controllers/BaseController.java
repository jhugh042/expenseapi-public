package com.hughesportal.controllers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Jacob Hughes
 *
 * Base controller class holds all the re-useable functions 
 * for the other controller classes
 */
@RestController
public class BaseController {

	public static final String BASE_URI="/api";

	@GetMapping(BASE_URI)
	public ResponseEntity<?> getAPIDoc(){
		File file = new File("expensesapi.html");
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String content ="";
			String line;
			while((line=br.readLine()) != null) {
				content += line + "\n";
			}
			return ResponseEntity.status(HttpStatus.OK)
					.header("Content-Type", "text/html")
					.body(content);
			
		} catch ( IOException e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.header("Content-Type", "text/html")
					.body("Error reading api file.");
		}
	}
}
