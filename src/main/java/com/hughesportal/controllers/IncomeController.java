package com.hughesportal.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hughesportal.entity.Incomes;
import com.hughesportal.services.IncomeService;

/**
 * @author Jacob Hughes
 * 
 * Controller for the master income enpoint, handles
 * all of the mappings and query parameters.
 */
@RestController
public class IncomeController {

	@Autowired
	IncomeService service;
	
	/**
	 * Get Master Income data
	 * @param year
	 * @param html
	 * @return ResponseEntity
	 */
	@GetMapping(BaseController.BASE_URI+"/masterIncome")
	public ResponseEntity<?> getMasterIncome(
					@RequestParam(required=true) Integer year,
					@RequestParam(required=false) Boolean html){
		return service.getMasterIncome(year, html);
	}
	
	/**
	 * Get all income data
	 * @param day
	 * @param gt
	 * @param lt
	 * @param month
	 * @param year
	 * @param account
	 * @param html
	 * @return ResponseEntity
	 */
	@GetMapping(BaseController.BASE_URI+"/masterIncome/incomes")
	public ResponseEntity<?> getAllIncomes(
			@RequestParam(required=false) Integer day,
			@RequestParam(required=false) Integer gt,
			@RequestParam(required=false) Integer lt,
			@RequestParam(required=false) String month,
			@RequestParam(required=false) Integer year,
			@RequestParam(required=false) Integer account,
			@RequestParam(required=false) Boolean html){
		return service.getAllIncomes(day, gt, lt, month, year, account, html);
	}
	
	/**
	 * Create a new income
	 * @param income
	 * @return ResponseEntity
	 */
	@PostMapping(BaseController.BASE_URI+"/masterIncome/incomes")
	public ResponseEntity<?> createIncome(
			@RequestBody Incomes income){
		return service.createIncome(income);
	}
	
	/**
	 * Create multiple new incomes
	 * @param incomes
	 * @return ResponseEntity
	 */
	@PostMapping(BaseController.BASE_URI+"/masterIncome/incomes/bulk")
	public ResponseEntity<?> createMultipleNewIncomes(
			@RequestBody List<Incomes> incomes){
		return service.createMultipleNewIncomes(incomes);
	}
	
	/**
	 * Get income data by ID
	 * @param id
	 * @param html
	 * @return ResponseEntity
	 */
	@GetMapping(BaseController.BASE_URI+"/masterIncome/incomes/{id}")
	public ResponseEntity<?> getIncomeByID(
			@PathVariable Long id,
			@RequestParam(required=false) Boolean html){
		return service.getIncomeByID(id, html);
	}
	
	/**
	 * Patch income data by ID
	 * @param id
	 * @param income
	 * @return ResponseEntity
	 */
	@PatchMapping(BaseController.BASE_URI+"/masterIncome/incomes/{id}")
	public ResponseEntity<?> updateIncomeByID(
			@PathVariable Long id,
			@RequestBody Incomes income){
		return service.updateIncomeByID(id, income);
	}
	
	/**
	 * Remove income data by ID
	 * @param id
	 * @return ResponseEntity
	 */
	@DeleteMapping(BaseController.BASE_URI+"/masterIncome/incomes/{id}")
	public ResponseEntity<?> removeIncomeByID(
			@PathVariable Long id){
		return service.removeIncomeByID(id);
	}
}
