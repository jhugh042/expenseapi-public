package com.hughesportal.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hughesportal.entity.Expenses;
import com.hughesportal.services.ExpenseService;

@RestController
public class ExpenseController{

	@Autowired
	ExpenseService service;
	
	/**
	 * Get Master Expense data
	 * @param year
	 * @param html
	 * @return ResponseEntity
	 */
	@GetMapping(BaseController.BASE_URI+"/masterExpense")
	public ResponseEntity<?> getMasterExpense(
			@RequestParam(required=true) Integer year,
			@RequestParam(required=false) Boolean html){
		return service.getMasterExpense(year, html);
	}
	
	/**
	 * Get All Expenses
	 * @param category
	 * @param day
	 * @param gt
	 * @param lt
	 * @param month
	 * @param year
	 * @param html
	 * @param account
	 * @return ResponseEntity
	 */
	@GetMapping(BaseController.BASE_URI+"/masterExpense/expenses")
	public ResponseEntity<?> getAllExpenses(
			@RequestParam(required=false) String category,
			@RequestParam(required=false) Integer day,
			@RequestParam(required=false) Integer gt,
			@RequestParam(required=false) Integer lt,
			@RequestParam(required=false) String month,
			@RequestParam(required=false) Integer year,
			@RequestParam(required=false) Boolean html,
			@RequestParam(required=false) Integer account) {
		return service.getAllExpenses(category, day, gt, lt, month, year, html, account);
	}
	
	/**
	 * Create a new expense
	 * @param expense
	 * @return ResponseEntity
	 */
	@PostMapping(BaseController.BASE_URI+"/masterExpense/expenses")
	public ResponseEntity<?> createNewExpense(@RequestBody Expenses expense){
		return service.createNewExpense(expense);
	}
	
	/**
	 * Create multiple new expenses
	 * @param expenses
	 * @return ResponseEntity
	 */
	@PostMapping(BaseController.BASE_URI+"/masterExpense/expenses/bulk")
	public ResponseEntity<?> createMultipleNewExpenses(@RequestBody List<Expenses> expenses){
		return service.createMultipleExpenses(expenses);
	}
	
	/**
	 * Get expense by ID
	 * @param id
	 * @param html
	 * @return ResponseEntity
	 */
	@GetMapping(BaseController.BASE_URI+"/masterExpense/expenses/{id}")
	public ResponseEntity<?> getExpenseByID(
			@PathVariable Long id,
			@RequestParam(required=false) Boolean html) {
		return service.getExpenseByID(id,html);
	}
	
	/**
	 * Patch the expense by ID
	 * @param id
	 * @param expense
	 * @return ResponseEntity
	 */
	@PatchMapping(BaseController.BASE_URI+"/masterExpense/expenses/{id}")
	public ResponseEntity<?> updateExpenseByID(
			@PathVariable Long id,
			@RequestBody Expenses expense) {
		return service.updateExpenseByID(id, expense);
	}
	
	/**
	 * Remove an expense by ID
	 * @param id
	 * @return ResponseEntity
	 */
	@DeleteMapping(BaseController.BASE_URI+"/masterExpense/expenses/{id}")
	public ResponseEntity<?> removeExpenseByID(
			@PathVariable Long id) {
		return service.removeExpenseByID(id);
	}
}
