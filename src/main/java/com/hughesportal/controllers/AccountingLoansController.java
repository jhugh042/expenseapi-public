package com.hughesportal.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hughesportal.entity.Loans;
import com.hughesportal.services.AccountingLoanService;

@RestController
public class AccountingLoansController {

	@Autowired
	AccountingLoanService service;
	
	/**
	 * Get all loans
	 * @param html
	 * @return ResponseEntity
	 */
	@GetMapping(BaseController.BASE_URI+"/accounting/loans")
	public ResponseEntity<?> getAllLoans(
			@RequestParam(required=false) Boolean html){
		return service.getAllLoans(html);
	}
	
	/**
	 * Create a new loan
	 * @param loan
	 * @return ResponseEntity
	 */
	@PostMapping(BaseController.BASE_URI+"/accounting/loans")
	public ResponseEntity<?> createNewLoan(
			@RequestBody Loans loan){
		return service.createNewLoan(loan);
	}
	
	/**
	 * Get Loan with the given ID
	 * @param id
	 * @param html
	 * @return ResponseEntity
	 */
	@GetMapping(BaseController.BASE_URI+"/accounting/loans/{id}")
	public ResponseEntity<?> getLoanByID(
			@PathVariable Long id,
			@RequestParam(required=false) Boolean html){
		return service.getLoanByID(id, html);
	}
	
	/**
	 * Update the loan with the given ID
	 * @param id
	 * @param loans
	 * @return ResponseEntity
	 */
	@PatchMapping(BaseController.BASE_URI+"/accounting/loans/{id}")
	public ResponseEntity<?> updateLoanByID(
			@PathVariable Long id,
			@RequestBody Loans loan){
		return service.updateLoanByID(id, loan);
	}
	
	/**
	 * Delete the loan with the given ID
	 * @param id
	 * @return ResponseEntity
	 */
	@DeleteMapping(BaseController.BASE_URI+"/accounting/loans/{id}")
	public ResponseEntity<?> deleteLoanByID(
			@PathVariable Long id){
		return service.deleteLoanByID(id);
	}
	
	@GetMapping(BaseController.BASE_URI+"/accounting/loans/{id}/estimate")
	public ResponseEntity<?> getLoanEstimate(
			@PathVariable Long id,
			@RequestParam(required=false) Boolean html,
			@RequestParam(required=false) Integer payment){
		return service.getLoanEstimate(id,html,payment);
	}
	
}
