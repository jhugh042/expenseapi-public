package com.hughesportal.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hughesportal.entity.Accounts;
import com.hughesportal.services.AccountingAccountsService;

@RestController
public class AccountingAccountsController {

	@Autowired
	AccountingAccountsService service;
	
	@GetMapping(BaseController.BASE_URI+"/accounting/accounts")
	public ResponseEntity<?> getAllAccounts(){
		return null;
	}
	
	@PostMapping(BaseController.BASE_URI+"/accounting/accounts")
	public ResponseEntity<?> createNewAccount(
			@RequestBody Accounts account){
		return null;
	}
	
	@GetMapping(BaseController.BASE_URI+"/accounting/accounts/{id}")
	public ResponseEntity<?> getAccountByID(
			@PathVariable Long id){
		return null;
	}
	
	@PatchMapping(BaseController.BASE_URI+"/accounting/accounts/{id}")
	public ResponseEntity<?> updateAccountByID(
			@PathVariable Long id,
			@RequestBody Accounts account){
		return null;
	}
	
	@DeleteMapping(BaseController.BASE_URI+"/accounting/accounts/{id}")
	public ResponseEntity<?> deleteAccountByID(
			@PathVariable Long id){
		return null;
	}
}
