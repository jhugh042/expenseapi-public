package com.hughesportal.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hughesportal.services.AccountingService;

@RestController
public class AccountingController {

	@Autowired
	AccountingService service;
	
	/**
	 * Get Accounting book data
	 * @param year
	 * @param month
	 * @param account
	 * @param html
	 * @return ResponseEntity
	 */
	@GetMapping(BaseController.BASE_URI+"/accounting")
	public ResponseEntity<?> getAccountingBook(
			@RequestParam(required=true) Integer year,
			@RequestParam(required=true) String month,
			@RequestParam(required=false) Integer account,
			@RequestParam(required=false) Boolean html){
		return service.getAccountingBook(year, month, account, html);
	}
}
