package com.hughesportal.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hughesportal.entity.Accounts;
import com.hughesportal.entity.Paystub;
import com.hughesportal.services.AccountingPaystubService;

/** 
 * @author Jacob Hughes
 *
 * Controller for the accounting enpoint, handles
 * all of the mappings and query parameters.
 */
@RestController
public class AccountingPaystubController{

	@Autowired
	AccountingPaystubService service;
	
	/**
	 * Get all paystubs
	 * @param year
	 * @param html
	 * @return ResponseEntity
	 */
	@GetMapping(BaseController.BASE_URI+"/accounting/paystubs")
	public ResponseEntity<?> getPayStubs(
			@RequestParam(required=true) Integer year,
			@RequestParam(required=false) Boolean html){
		return service.getPayStubs(year,html);
	}
	
	/**
	 * Create a new paystub
	 * @param paystub
	 * @param createIncome
	 * @param account
	 * @return ResponseEntity
	 */
	@PostMapping(BaseController.BASE_URI+"/accounting/paystubs")
	public ResponseEntity<?> createPayStub(
			@RequestBody Paystub paystub,
			@RequestParam(required=false) Boolean createIncome,
			@RequestParam(required=false) Integer account){
		return service.createPayStub(paystub,createIncome,account);
	}

	/**
	  * get a paystub by ID
	  * @param id
	  * @param html
	  * @return ResponseEntity
	  */
	@GetMapping(BaseController.BASE_URI+"/accounting/paystubs/{id}")
	public ResponseEntity<?> getPayStubByID(
			@PathVariable Long id,
			@RequestParam(required=false) Boolean html){
		return service.getPayStubByID(id, html);
	}
	
	/**
	 * Patch the paystub with the ID
	 * @param id
	 * @param paystub
	 * @return ResponseEntity
	 */
	@PatchMapping(BaseController.BASE_URI+"/accounting/paystubs/{id}")
	public ResponseEntity<?> updatePayStubByID(
			@PathVariable Long id, 
			@RequestBody Paystub paystub){
		return service.updatePayStubByID(id, paystub);
	}
	
	/**
	 * Remove paystub with ID
	 * @param id
	 * @return ResponseEntity
	 */
	@DeleteMapping(BaseController.BASE_URI+"/accounting/paystubs/{id}")
	public ResponseEntity<?> removePayStubByID(
				@PathVariable Long id){
		return service.removePayStubByID(id);
	}
	
	
}
