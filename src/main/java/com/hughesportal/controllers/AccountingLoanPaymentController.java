package com.hughesportal.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hughesportal.entity.Loanpayment;
import com.hughesportal.services.AccountingLoanPaymentService;

@RestController
public class AccountingLoanPaymentController {

	@Autowired
	AccountingLoanPaymentService service;
	
	@GetMapping(BaseController.BASE_URI+"/accounting/loans/{id}/loanPayment")
	public ResponseEntity<?> getAllLoanPayments(
			@PathVariable Long id,
			@RequestParam(required=false) Boolean html){
		return service.getAllLoanPayments(id, html);
	}
	
	@PostMapping(BaseController.BASE_URI+"/accounting/loans/{id}/loanPayment")
	public ResponseEntity<?> createNewLoanPayment(
			@PathVariable Long id,
			@RequestBody Loanpayment loanPayment){
		return service.createNewLoanPayment(id, loanPayment);
	}
	
	@GetMapping(BaseController.BASE_URI+"/accounting/loans/{id}/loanPayment/{pid}")
	public ResponseEntity<?> getLoanPaymentByID(
			@PathVariable Long id,
			@PathVariable Long pid,
			@RequestParam(required=false) Boolean html){
		return service.getLoanPaymentByID(id, pid, html);
	}
	
	@PatchMapping(BaseController.BASE_URI+"/accounting/loans/{id}/loanPayment/{pid}")
	public ResponseEntity<?> updateLoanPaymentByID(
			@PathVariable Long id,
			@PathVariable Long pid,
			@RequestBody Loanpayment loanPayment){
		return service.updateLoanPaymentByID(id, pid, loanPayment);
	}
	
	@DeleteMapping(BaseController.BASE_URI+"/accounting/loans/{id}/loanPayment/{pid}")
	public ResponseEntity<?> deleteLoanPaymentByID(
			@PathVariable Long id,
			@PathVariable Long pid){
		return service.deleteLoanPaymentByID(id, pid);
	}
}
