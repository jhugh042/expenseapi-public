package com.hughesportal.objects;

/**
 * @author Jacob Hughes
 *
 * Special object used for Master endpoints
 */
public class Months {

	private Double jan = 0.00;
	private Double feb = 0.00;
	private Double march = 0.00;
	private Double april = 0.00;
	private Double may = 0.00;
	private Double june = 0.00;
	private Double july = 0.00;
	private Double aug = 0.00;
	private Double sept = 0.00;
	private Double oct = 0.00;
	private Double nov = 0.00;
	private Double dec = 0.00;
	
	public Months(){}
	
	/**
	 * 
	 * @param month
	 * @param value
	 */
	public Months(String month, Double value) {
		setData(month,value);
	}
	
	/**
	 * Sets the correct variable depending on the month
	 * @param month
	 * @param value
	 */
	public void setData(String month, Double value) {
			if(month.equals("January")){
				jan = value;
			}
			else if(month.equals("Febuary")){
				feb = value;
			}
			else if(month.equals("March")){
				march = value;
			}
			else if(month.equals("April")) {
				april = value;
			}
			else if(month.equals("May")){
				may = value;
			}
			else if(month.equals("June")){
				june = value;
			}
			else if(month.equals("July")){
				july = value;
			}
			else if(month.equals("August")){
				aug = value;
			}
			else if(month.equals("September")){
				sept = value;
			}
			else if(month.equals("October")){
				oct = value;
			}
			else if(month.equals("November")){
				nov = value;
			}
			else if(month.equals("December")){
				dec = value;
			}
	}

	/**
	 * @return the jan
	 */
	public Double getJan() {
		return jan;
	}

	/**
	 * @param jan the jan to set
	 */
	public void setJan(Double jan) {
		this.jan = jan;
	}

	/**
	 * @return the feb
	 */
	public Double getFeb() {
		return feb;
	}

	/**
	 * @param feb the feb to set
	 */
	public void setFeb(Double feb) {
		this.feb = feb;
	}

	/**
	 * @return the march
	 */
	public Double getMarch() {
		return march;
	}

	/**
	 * @param march the march to set
	 */
	public void setMarch(Double march) {
		this.march = march;
	}

	/**
	 * @return the april
	 */
	public Double getApril() {
		return april;
	}

	/**
	 * @param april the april to set
	 */
	public void setApril(Double april) {
		this.april = april;
	}

	/**
	 * @return the may
	 */
	public Double getMay() {
		return may;
	}

	/**
	 * @param may the may to set
	 */
	public void setMay(Double may) {
		this.may = may;
	}

	/**
	 * @return the june
	 */
	public Double getJune() {
		return june;
	}

	/**
	 * @param june the june to set
	 */
	public void setJune(Double june) {
		this.june = june;
	}

	/**
	 * @return the july
	 */
	public Double getJuly() {
		return july;
	}

	/**
	 * @param july the july to set
	 */
	public void setJuly(Double july) {
		this.july = july;
	}

	/**
	 * @return the aug
	 */
	public Double getAug() {
		return aug;
	}

	/**
	 * @param aug the aug to set
	 */
	public void setAug(Double aug) {
		this.aug = aug;
	}

	/**
	 * @return the sept
	 */
	public Double getSept() {
		return sept;
	}

	/**
	 * @param sept the sept to set
	 */
	public void setSept(Double sept) {
		this.sept = sept;
	}

	/**
	 * @return the oct
	 */
	public Double getOct() {
		return oct;
	}

	/**
	 * @param oct the oct to set
	 */
	public void setOct(Double oct) {
		this.oct = oct;
	}

	/**
	 * @return the nov
	 */
	public Double getNov() {
		return nov;
	}

	/**
	 * @param nov the nov to set
	 */
	public void setNov(Double nov) {
		this.nov = nov;
	}

	/**
	 * @return the dec
	 */
	public Double getDec() {
		return dec;
	}

	/**
	 * @param dec the dec to set
	 */
	public void setDec(Double dec) {
		this.dec = dec;
	}

	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		Double total = jan+feb+march+april+may+june+july+aug+sept+oct+nov+dec;
		return String.format(
				"<td>$%.2f</td>"
			  + "<td>$%.2f</td>"
			  + "<td>$%.2f</td>"
			  + "<td>$%.2f</td>"
			  + "<td>$%.2f</td>"
			  + "<td>$%.2f</td>"
			  + "<td>$%.2f</td>"
			  + "<td>$%.2f</td>"
			  + "<td>$%.2f</td>"
			  + "<td>$%.2f</td>"
			  + "<td>$%.2f</td>"
			  + "<td>$%.2f</td>"
			  + "<td>$%.2f</td>",
			  
				jan,feb,march,april,may,june,july,aug,sept,oct,nov,dec,total);
	}
	
	
}
