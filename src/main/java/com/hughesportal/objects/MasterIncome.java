package com.hughesportal.objects;

/**
 * @author Jacob Hughes
 *
 * Special object used for MasterIncome endpoint
 * has all month data
 */
public class MasterIncome {

	private Integer account;
	private Months months;
	
	MasterIncome(){}

	/**
	 * @param account
	 * @param months
	 */
	public MasterIncome(Integer account, Months months) {
		this.account = account;
		this.months = months;
	}

	/**
	 * @return the account
	 */
	public Integer getAccount() {
		return account;
	}

	/**
	 * @param account the account to set
	 */
	public void setAccount(Integer account) {
		this.account = account;
	}

	/**
	 * @return the months
	 */
	public Months getMonths() {
		return months;
	}

	/**
	 * @param months the months to set
	 */
	public void setMonths(Months months) {
		this.months = months;
	}
	
	public static String getTitle() {
		return "<tr>"
			+ "<th>Account</th>"
			+ "<th>January</th>"
			+ "<th>Febuary</th>"
			+ "<th>March</th>"
			+ "<th>April</th>"
			+ "<th>May</th>"
			+ "<th>June</th>"
			+ "<th>July</th>"
			+ "<th>August</th>"
			+ "<th>September</th>"
			+ "<th>October</th>"
			+ "<th>November</th>"
			+ "<th>December</th>"
			+ "<th>Total</th>"
			+ "</tr>";
	}
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String value;
		value = "<tr>";
		value += String.format("<td>%d</td>", account);
		value += months.toString();
		value += "</tr>";
		return value;
	}
	
	
}
