package com.hughesportal.objects;

/**
 * @author Jacob Hughes
 *
 * Special object for accounting enpoint
 */
public class Accounting {

	private Integer account;
	private Double expense = 0.00;
	private Double income = 0.00;
	
	Accounting(){}

	/**
	 * @param account
	 * @param ammount
	 */
	public Accounting(Integer account, Double ammount, Boolean expense) {
		this.account = account;
		if(expense) {
			this.expense = ammount;
		}else {
			this.income = ammount;
		}
	}

	/**
	 * @return the account
	 */
	public Integer getAccount() {
		return account;
	}

	/**
	 * @param account the account to set
	 */
	public void setAccount(Integer account) {
		this.account = account;
	}

	/**
	 * @return the expense
	 */
	public Double getExpense() {
		return expense;
	}

	/**
	 * @param expense the expense to set
	 */
	public void setExpense(Double expense) {
		this.expense = expense;
	}

	/**
	 * @return the income
	 */
	public Double getIncome() {
		return income;
	}

	/**
	 * @param income the income to set
	 */
	public void setIncome(Double income) {
		this.income = income;
	}
	
	public static String getTitle() {
		return "<tr>"
			+ "<th>Account</th>"
			+ "<th>Expense</th>"
			+ "<th>Income</th>"
			+ "</tr>";
	}
	
	@Override
	public String toString() {
		return String.format("<tr>"
							+ "<td>%d</td>"
							+ "<td>%.2f</td>"
							+ "<td>%.2f</td>"
							+ "</tr>",
				account,expense,income);
	}
	
	
}
