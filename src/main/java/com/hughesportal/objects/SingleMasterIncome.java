package com.hughesportal.objects;

/**
 * @author Jacob Hughes
 *
 * Special object used for MasterIncome endpoint
 * has single month object
 */
public class SingleMasterIncome {

	private Double ammount;
	private Integer account;
	private String month;
	
	SingleMasterIncome(){}

	/**
	 * @param ammount
	 * @param account
	 * @param month
	 */
	public SingleMasterIncome(Double ammount, Integer account, String month) {
		this.ammount = ammount;
		this.account = account;
		this.month = month;
	}

	/**
	 * @return the ammount
	 */
	public Double getAmmount() {
		return ammount;
	}

	/**
	 * @param ammount the ammount to set
	 */
	public void setAmmount(Double ammount) {
		this.ammount = ammount;
	}

	/**
	 * @return the account
	 */
	public Integer getAccount() {
		return account;
	}

	/**
	 * @param account the account to set
	 */
	public void setAccount(Integer account) {
		this.account = account;
	}

	/**
	 * @return the month
	 */
	public String getMonth() {
		return month;
	}

	/**
	 * @param month the month to set
	 */
	public void setMonth(String month) {
		this.month = month;
	}
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("Expense[Ammount=%.2f, Account=%d, Month=%s]",
				ammount,account,month);
	}
	
	
}
