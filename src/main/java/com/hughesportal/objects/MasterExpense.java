package com.hughesportal.objects;

/**
 * @author Jacob Hughes
 *
 * Special object used for MasterExpense endpoint
 * has all month data
 */
public class MasterExpense {

	private String category;
	private Months months;
	
	MasterExpense(){}
	
	/**
	 * @param category
	 * @param months
	 */
	public MasterExpense(String category, Months months) {
		this.category = category;
		this.months = months;
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return the months
	 */
	public Months getMonths() {
		return months;
	}

	/**
	 * @param months the months to set
	 */
	public void setMonths(Months months) {
		this.months = months;
	}

	public static String getTitle() {
		return "<tr>"
			+ "<th>Category</th>"
			+ "<th>January</th>"
			+ "<th>Febuary</th>"
			+ "<th>March</th>"
			+ "<th>April</th>"
			+ "<th>May</th>"
			+ "<th>June</th>"
			+ "<th>July</th>"
			+ "<th>August</th>"
			+ "<th>September</th>"
			+ "<th>October</th>"
			+ "<th>November</th>"
			+ "<th>December</th>"
			+ "<th>Total</th>"
			+ "</tr>";
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String value;
		value = "<tr>";
		value += String.format("<td>%s</td>", category);
		value += months.toString();
		value += "</tr>";
		return value;
	}
	
	
	
}
