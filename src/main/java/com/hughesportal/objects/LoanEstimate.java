package com.hughesportal.objects;

public class LoanEstimate {

	private String date;
	private Double payment;
	private Double interest;
	private Double interestYTD;
	private Double principal;
	private Double principalYTD;
	private Double balance;
	
	protected LoanEstimate() {}

	/**
	 * @param date
	 * @param payment
	 * @param interest
	 * @param interestYTD
	 * @param principal
	 * @param principalYTD
	 * @param balance
	 */
	public LoanEstimate(String date, Double payment, Double interest, Double interestYTD, Double principal,
			Double principalYTD, Double balance) {
		this.date = date;
		this.payment = payment;
		this.interest = interest;
		this.interestYTD = interestYTD;
		this.principal = principal;
		this.principalYTD = principalYTD;
		this.balance = balance;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @return the payment
	 */
	public Double getPayment() {
		return payment;
	}

	/**
	 * @param payment the payment to set
	 */
	public void setPayment(Double payment) {
		this.payment = payment;
	}

	/**
	 * @return the interest
	 */
	public Double getInterest() {
		return interest;
	}

	/**
	 * @param interest the interest to set
	 */
	public void setInterest(Double interest) {
		this.interest = interest;
	}

	/**
	 * @return the interestYTD
	 */
	public Double getInterestYTD() {
		return interestYTD;
	}

	/**
	 * @param interestYTD the interestYTD to set
	 */
	public void setInterestYTD(Double interestYTD) {
		this.interestYTD = interestYTD;
	}

	/**
	 * @return the principal
	 */
	public Double getPrincipal() {
		return principal;
	}

	/**
	 * @param principal the principal to set
	 */
	public void setPrincipal(Double principal) {
		this.principal = principal;
	}

	/**
	 * @return the principalYTD
	 */
	public Double getPrincipalYTD() {
		return principalYTD;
	}

	/**
	 * @param principalYTD the principalYTD to set
	 */
	public void setPrincipalYTD(Double principalYTD) {
		this.principalYTD = principalYTD;
	}

	/**
	 * @return the balance
	 */
	public Double getBalance() {
		return balance;
	}

	/**
	 * @param balance the balance to set
	 */
	public void setBalance(Double balance) {
		this.balance = balance;
	}
	
	public static String getTitle() {
		return "<tr>"
			 + "<th>Date</th>"
			 + "<th>Payment</th>"
			 + "<th>Interest</th>"
			 + "<th>Interest YTD</th>"
			 + "<th>Principal</th>"
			 + "<th>Principal YTD</th>"
			 + "<th>Balance</th>"
			 + "</tr>";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("<tr>"
						   + "<td>%s</td>"
						   + "<td>%.2f</td>"
						   + "<td>%.2f</td>"
						   + "<td>%.2f</td>"
						   + "<td>%.2f</td>"
						   + "<td>%.2f</td>"
						   + "<td>%.2f</td>"
						   + "</tr>",
						   date,payment,interest,interestYTD,
						   principal,principalYTD,balance);
	}
	
	
	
}
