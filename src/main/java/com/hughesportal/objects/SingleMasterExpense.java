package com.hughesportal.objects;

/**
 * @author Jacob Hughes
 *
 * Special object used for MasterExpense endpoint
 * has single month object
 */
public class SingleMasterExpense {

	private Double ammount;
	private String category;
	private String month;
	
	SingleMasterExpense(){}
	
	/**
	 * @param ammount
	 * @param category
	 * @param month
	 */
	public SingleMasterExpense(Double ammount, String category, String month) {
		this.ammount = ammount;
		this.category = category;
		this.month = month;
	}

	/**
	 * @return the ammount
	 */
	public Double getAmmount() {
		return ammount;
	}

	/**
	 * @param ammount the ammount to set
	 */
	public void setAmmount(Double ammount) {
		this.ammount = ammount;
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return the month
	 */
	public String getMonth() {
		return month;
	}

	/**
	 * @param month the month to set
	 */
	public void setMonth(String month) {
		this.month = month;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("Expense[Ammount=%.2f, Category=%s, Month=%s]",
				ammount,category,month);
	}
	
	
	
	
}
