/*==================================Loan====================================*/

INSERT INTO Loans(name,ammount,interest,loan_start_month,loan_start_year,loan_end_month,loan_end_year)
VALUES ('Student Loan',80000.00,6.00,'June',2019,'June',2029);

INSERT INTO Loans(name,ammount,interest,loan_start_month,loan_start_year,loan_end_month,loan_end_year)
VALUES ('Student Loan',50000.00,6.00,'June',2019,'June',2029);

/*==============================Loan payment==============================*/

INSERT INTO LoanPayment(account,ammount,month,year,loan_id)
VALUES (1,1200.00,'June',2019,1)
