package com.hughesportal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;

import com.hughesportal.entity.Expenses;
import com.hughesportal.entity.Incomes;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient
public class ExpenseIntegrationTest{

	@SuppressWarnings("unused")
	private static Logger logger = LoggerFactory.getLogger(ExpenseIntegrationTest.class);
	
	@Autowired
	private WebTestClient webClient;
	
	@Test
	public void getMasterExpenseTest() {
		this.webClient
			.get()
			.uri("/api/masterExpense?year=2019")
			.exchange().expectStatus().isOk()
			.expectBodyList(Expenses.class);
	}

	@Test
	public void getAllExpensesTest() {
		this.webClient
			.get()
			.uri("/api/masterExpense/expenses")
			.exchange().expectStatus().isOk()
			.expectBodyList(Expenses.class);
		
		this.webClient
			.get()
			.uri("/api/masterExpense/expenses"
				+ "?category=Food&day=22&gt=3&lt=5"
				+ "&month=April&year=2019")
			.exchange().expectStatus().isOk()
			.expectBodyList(Expenses.class);
		
		this.webClient
		.get()
		.uri("/api/masterExpense/expenses"
			+ "?account=1")
		.exchange().expectStatus().isOk();
	}
	
	@Test
	public void createNewExpenseTest() {
		
		String request = 
				"{\r\n" + 
				"  \"category\": \"Test\",\r\n" + 
				"  \"ammount\": 15.00,\r\n" + 
				"  \"account\": 1,\r\n" + 
				"  \"year\": 2020,\r\n" + 
				"  \"month\": \"May\",\r\n" + 
				"  \"day\": 15\r\n" + 
				"}";
		
		this.webClient
		.post()
		.uri("/api/masterExpense/expenses")
		.contentType(MediaType.APPLICATION_JSON_UTF8)
		.body(BodyInserters.fromObject(request))
		.exchange().expectStatus().is2xxSuccessful();
	}
	
	@Test
	public void createMultipleExpensesTest() {
		String request = 
				"[{\r\n" + 
				"  \"category\": \"Test\",\r\n" + 
				"  \"ammount\": 15.00,\r\n" + 
				"  \"account\": 1,\r\n" + 
				"  \"year\": 2020,\r\n" + 
				"  \"month\": \"May\",\r\n" + 
				"  \"day\": 15\r\n" + 
				"},"
				+ "{\r\n" + 
				"  \"category\": \"Test\",\r\n" + 
				"  \"ammount\": 15.00,\r\n" + 
				"  \"account\": 1,\r\n" + 
				"  \"year\": 2020,\r\n" + 
				"  \"month\": \"May\",\r\n" + 
				"  \"day\": 15\r\n" + 
				"}"
				+ "]" ;
		
		this.webClient
		.post()
		.uri("/api/masterExpense/expenses/bulk")
		.contentType(MediaType.APPLICATION_JSON_UTF8)
		.body(BodyInserters.fromObject(request))
		.exchange().expectStatus().is2xxSuccessful();
	}
	
	@Test
	public void getExpenseByIDTest() {
		this.webClient
		.get()
		.uri("/api/masterExpense/expenses/1")
		.exchange().expectStatus().isOk()
		.expectBodyList(Expenses.class);
	}
	
	@Test
	public void updateExpenseByIDTest() {
		String request = 
				"{\r\n" + 
				"  \"category\": \"Test\"\r\n" + 
				"}";
		this.webClient
			.patch()
			.uri("/api/masterExpense/expenses/1")
			.contentType(MediaType.APPLICATION_JSON_UTF8)
			.body(BodyInserters.fromObject(request))
			.exchange().expectStatus().is2xxSuccessful();
	}
	
	@Test
	public void removeExpenseByIDTest() {
		this.webClient
			.delete()
			.uri("/api/masterExpense/expenses/2")
			.exchange().expectStatus().is2xxSuccessful();
	}
	
	
	
}
