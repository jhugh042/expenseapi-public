package com.hughesportal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;

import com.hughesportal.entity.Expenses;
import com.hughesportal.entity.Incomes;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient
public class IncomeIntegrationTest {

	@SuppressWarnings("unused")
	private static Logger logger = LoggerFactory.getLogger(ExpenseIntegrationTest.class);
	
	@Autowired
	private WebTestClient webClient;
	

	@Test
	public void getMasterIncomeTest() {
		this.webClient
		.get()
		.uri("/api/masterIncome?year=2019")
		.exchange().expectStatus().isOk()
		.expectBodyList(Incomes.class);
	}
	
	@Test
	public void getAllIncomesTest() {
		this.webClient
			.get()
			.uri("/api/masterIncome/incomes")
			.exchange().expectStatus().isOk()
			.expectBodyList(Incomes.class);
		
		//('Checking','Food','April',2019,22,4.90);
		this.webClient
			.get()
			.uri("/api/masterIncome/incomes"
				+ "?day=17&month=May&year=2019")
			.exchange().expectStatus().isOk()
			.expectBodyList(Expenses.class);
	}
	
	@Test
	public void createNewIncomeTest() {
		
		String request = 
				"{\r\n" + 
				"  \"ammount\": 15.00,\r\n" + 
				"  \"account\": 1,\r\n" + 
				"  \"year\": 2020,\r\n" + 
				"  \"month\": \"May\",\r\n" + 
				"  \"day\": 15\r\n" + 
				"}";
		
		this.webClient
		.post()
		.uri("/api/masterIncome/incomes")
		.contentType(MediaType.APPLICATION_JSON_UTF8)
		.body(BodyInserters.fromObject(request))
		.exchange().expectStatus().is2xxSuccessful();
	}
	
	@Test
	public void createMultipleIncomesTest() {
		String request = 
				"[{\r\n" + 
				"  \"ammount\": 15.00,\r\n" + 
				"  \"account\": 1,\r\n" + 
				"  \"year\": 2020,\r\n" + 
				"  \"month\": \"May\",\r\n" + 
				"  \"day\": 15\r\n" + 
				"},"
				+ "{\r\n" + 
				"  \"ammount\": 15.00,\r\n" + 
				"  \"account\": 1,\r\n" + 
				"  \"year\": 2020,\r\n" + 
				"  \"month\": \"May\",\r\n" + 
				"  \"day\": 15\r\n" + 
				"}"
				+ "]" ;
		
		this.webClient
		.post()
		.uri("/api/masterIncome/incomes/bulk")
		.contentType(MediaType.APPLICATION_JSON_UTF8)
		.body(BodyInserters.fromObject(request))
		.exchange().expectStatus().is2xxSuccessful();
	}
	
	@Test
	public void getIncomeByIDTest() {
		this.webClient
		.get()
		.uri("/api/masterIncome/incomes/1")
		.exchange().expectStatus().isOk()
		.expectBodyList(Incomes.class);
	}
	
	@Test
	public void updateIncomeByIDTest() {
		String request = 
				"{\r\n" + 
				"  \"account\": 1\r\n" + 
				"}";
		this.webClient
			.patch()
			.uri("/api/masterIncome/incomes/1")
			.contentType(MediaType.APPLICATION_JSON_UTF8)
			.body(BodyInserters.fromObject(request))
			.exchange().expectStatus().is2xxSuccessful();
	}
	
	@Test
	public void removeIncomeByIDTest() {
		this.webClient
			.delete()
			.uri("/api/masterIncome/incomes/2")
			.exchange().expectStatus().is2xxSuccessful();
	}
	
	
}
