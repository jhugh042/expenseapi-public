package com.hughesportal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;

import com.hughesportal.entity.Loanpayment;
import com.hughesportal.entity.Loans;
import com.hughesportal.entity.Paystub;
import com.hughesportal.objects.Accounting;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient
public class AccountingIntergationTest {

	@SuppressWarnings("unused")
	private static Logger logger = LoggerFactory.getLogger(ExpenseIntegrationTest.class);
	
	@Autowired
	private WebTestClient webClient;
	
	/***********************AccoutningService**********************************/
	
	@Test
	public void getAccountingBookTest() {
		this.webClient
		.get()
		.uri("/api/accounting?year=2019&month=May")
		.exchange().expectStatus().isOk()
		.expectBodyList(Accounting.class);
	}
	
	
	/*******************AccountingPaystubService************************/
	@Test
	public void getPayStubsTest() {
		this.webClient
		.get()
		.uri("/api/accounting/paystubs?year=2019")
		.exchange().expectStatus().isOk()
		.expectBodyList(Accounting.class);
	}
	
	@Test
	public void createPayStubTest() {
		String request = "{\r\n" + 
				"  \"grossPay\": 0.00,\r\n" + 
				"  \"fedTax\": 0.00,\r\n" + 
				"  \"stateTax\": 0.00,\r\n" + 
				"  \"ssnTax\": 0.00,\r\n" + 
				"  \"medicare\": 0.00,\r\n" + 
				"  \"retirement\": 0.00,\r\n" + 
				"  \"netPay\": 0.00,\r\n" + 
				"  \"year\": 2019,\r\n" + 
				"  \"month\": \"May\",\r\n" + 
				"  \"day\": 19\r\n" + 
				"}";
		
		this.webClient
		.post()
		.uri("/api/accounting/paystubs")
		.contentType(MediaType.APPLICATION_JSON_UTF8)
		.body(BodyInserters.fromObject(request))
		.exchange().expectStatus().is2xxSuccessful();
		
	}
	
	@Test
	public void getPayStubByIDTest() {
		this.webClient
		.get()
		.uri("/api/accounting/paystubs/1")
		.exchange().expectStatus().isOk()
		.expectBodyList(Paystub.class);
	}
	
	@Test
	public void updatePayStubByIDTest() {
		String request = 
				"{\r\n" + 
				"  \"netPay\": 10.00\r\n" + 
				"}";
		this.webClient
			.patch()
			.uri("/api/accounting/paystubs/1")
			.contentType(MediaType.APPLICATION_JSON_UTF8)
			.body(BodyInserters.fromObject(request))
			.exchange().expectStatus().is2xxSuccessful();
	}
	
	@Test
	public void removePayStubByIDTest() {
		this.webClient
		.delete()
		.uri("/api/accounting/paystubs/2")
		.exchange().expectStatus().is2xxSuccessful();
	}
	
	/******************AccountingLoanService********************/
	
	@Test
	public void getAllLoansTest() {
		this.webClient
		.get()
		.uri("/api/accounting/loans")
		.exchange().expectStatus().isOk()
		.expectBodyList(Loans.class);
	}
	
	
	@Test
	public void createLoanTest() {
		String request = "{\r\n" + 
				"  \"name\": \"Test\",\r\n" + 
				"  \"ammount\": 100.00,\r\n"+
				"  \"interest\": 3.00,\r\n" + 
				"  \"loanStartMonth\": \"May\",\r\n" + 
				"  \"loanStartYear\": 2019,\r\n" + 
				"  \"loanEndMonth\": \"May\",\r\n" + 
				"  \"loanEndYear\": 2029\r\n" + 
				"}";
		
		this.webClient
		.post()
		.uri("/api/accounting/loans")
		.contentType(MediaType.APPLICATION_JSON_UTF8)
		.body(BodyInserters.fromObject(request))
		.exchange().expectStatus().is2xxSuccessful();
		
	}
	
	@Test
	public void getLoanByIDTest() {
		this.webClient
		.get()
		.uri("/api/accounting/loans/1")
		.exchange().expectStatus().isOk()
		.expectBodyList(Loans.class);
	}
	
	@Test
	public void updateLoanByIDTest() {
		String request = 
				"{\r\n" + 
				"  \"ammount\": 10.00\r\n" + 
				"}";
		this.webClient
			.patch()
			.uri("/api/accounting/loans/1")
			.contentType(MediaType.APPLICATION_JSON_UTF8)
			.body(BodyInserters.fromObject(request))
			.exchange().expectStatus().is2xxSuccessful();
	}
	
	@Test
	public void removeLoanByIDTest() {
		this.webClient
		.delete()
		.uri("/api/accounting/loans/2")
		.exchange().expectStatus().is2xxSuccessful();
	}
	
	/*====================AccountingLoanPaymentService================*/
	
	@Test
	public void getAllLoanPaymentsTest() {
		this.webClient
		.get()
		.uri("/api/accounting/loans/1/loanPayment")
		.exchange().expectStatus().isOk()
		.expectBodyList(Loanpayment.class);
	}
	
	
	@Test
	public void createLoanPaymentTest() {
		String request = "{\r\n" + 
				"  \"account\": 1,\r\n" + 
				"  \"ammount\": 1000.00,\r\n" + 
				"  \"month\": \"May\",\r\n" + 
				"  \"year\": 2019,\r\n" + 
				"  \"loanId\": 1\r\n" + 
				"}";
		
		this.webClient
		.post()
		.uri("/api/accounting/loans/1/loanPayment")
		.contentType(MediaType.APPLICATION_JSON_UTF8)
		.body(BodyInserters.fromObject(request))
		.exchange().expectStatus().is2xxSuccessful();
		
	}
	
	@Test
	public void getLoanPaymentByIDTest() {
		this.webClient
		.get()
		.uri("/api/accounting/loans/1/loanPayment/1")
		.exchange().expectStatus().isOk()
		.expectBodyList(Loanpayment.class);
	}
	
	@Test
	public void updateLoanPaymentByIDTest() {
		String request = 
				"{\r\n" + 
				"  \"ammount\": 10.00\r\n" + 
				"}";
		this.webClient
			.patch()
			.uri("/api/accounting/loans/1/loanPayment/1")
			.contentType(MediaType.APPLICATION_JSON_UTF8)
			.body(BodyInserters.fromObject(request))
			.exchange().expectStatus().is2xxSuccessful();
	}
	
	@Test
	public void removeLoanPaymentByIDTest() {
		this.webClient
		.delete()
		.uri("/api/accounting/loans/1/loanPayment/2")
		.exchange().expectStatus().is2xxSuccessful();
	}
}
